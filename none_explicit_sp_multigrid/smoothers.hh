#ifndef SMOOTHERS_HH
#define SMOOTHERS_HH
#include <map>
#include <dune/istl/eigenvalue/poweriteration.hh>
template<class MatrixType, class VectorType>
class RichardsonIterationSmoother
{
private:
    size_t nu;
    std::map<size_t, std::map<size_t, double> > map_eigen;
    template <typename M>
    double calcEigenvalues(M &matrix, size_t j,size_t i)
    {
        if (map_eigen.count(j) > 0)
        { //return early if we already have the stiffness Matrix calculated
            if(map_eigen[j].count(i)>0)
            return map_eigen.at(j).at(i);
        } else {
            map_eigen[j]= std::map<size_t,double>();
        }
        auto result = Dune::PowerIteration_Algorithms<M, VectorType>(matrix, 20000, 1);
        double dominant;
        VectorType ev;
        ev.resize(matrix.N());
        ev[0]=1.0;
        //1e-1 exppecting huge eigenvalues
        result.applyPowerIteration(3e-4, ev, dominant);
        std::cout << dominant << std::endl;
        // std::cin>> dominant;
        map_eigen[j][i] = dominant;
        return dominant;
    }
public:
RichardsonIterationSmoother(size_t _nu):nu(_nu){}
    template <class V, class M>
    void apply(V &xk_j, V& b_j,M &A_j,size_t j, size_t i)
    {
        auto omega = 1.8 / calcEigenvalues(A_j,j,i);
        VectorType def = b_j;
        std::cout<<"A_J"<<A_j.M()<<" "<<A_j.N()<<std::endl;
        std::cout << "def"<<def.size()<<std::endl;
        for (size_t k = 0; k < nu; k++)
        {
             def = b_j; //Be sure to copy here
            A_j.mmv(xk_j, def);
            def *= omega;
            if(k%10==0){
                std::cout<<"||def||_2:"<<def.two_norm()<<std::endl;
            }
            xk_j += (def); //richardson iteration pre smoothing
        }
    }
};

template<class Smoother, class MT>
class SmootherProxy{
    std::map<std::pair<int, int>, Smoother> store;
     std::function<Smoother(const MT &matrix,int j, int i)> factory;
    public:
    SmootherProxy( std::function<Smoother(const MT &matrix,int j, int i)> _factory):factory(_factory){

    }
    template <class V, class M>
     void apply(V &xk_j, V b_j,M &A_j,size_t space_j,int time_i)
    {
        if (store.find(std::make_pair(time_i, space_j)) != store.end())
        {
            //std::cout << "Delivering:" << time_i << std::endl;
            store.at(std::make_pair(time_i, space_j)).apply(xk_j,b_j);
            return;
        }
        auto a = factory(A_j,space_j,time_i);
        store.insert(std::make_pair(std::make_pair(time_i, space_j), a));
        a.apply(xk_j,b_j);
    }
};
#endif