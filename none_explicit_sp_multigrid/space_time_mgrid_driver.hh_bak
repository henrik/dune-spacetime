#ifndef SPACE_TIME_MGRID_DRIVER_HH
#define SPACE_TIME_MGRID_DRIVER_HH
#include <dune/istl/umfpack.hh>
#include "prolongation.hh"
//TODO: 1. Time coarsening --> Reorganize Grid and matrix hierarchy
// 2. Full Coarse --> dito
// Solve with p_t=0 first, later dynamically
// Uses ThreadJobOrganizer to run in parallel index is two dimensional to account for time in first and space in second parameter
//this is based on parsolve MultigridOnOverlappinggrids
template <class Basis, class MatrixType, class VectorType, class PreSmoother, class PostSmoother, typename ctype>
class SpaceTimeMultiGridMethodDriver
{
    int J;
    ctype mu_critical;
    std::function<ctype(size_t)> tau_L;
    std::function<ctype(size_t)> h_L;
    std::map<std::pair<int, int>, MatrixType> store;
    std::map<std::pair<int, int>, PreSmoother> pre_store;
    std::map<std::pair<int, int>, PostSmoother> post_store;
public:
    typedef std::function<MatrixType(int, int)> ProblemFactory;
    typedef std::function<PreSmoother(MatrixType,int, int)> PreSmootherFactory;
    typedef std::function<PostSmoother(MatrixType,int, int)> PostSmootherFactory;
    typedef typename Basis::GridView BGV;
    typedef std::function<const BGV(int)> BasisFactory;
    const BasisFactory &B;
    //    size_t nu_1, nu_2;
    const ProblemFactory &__A;
    const PreSmootherFactory & __pre;
    const PostSmootherFactory & __post;
    SpaceTimeMultiGridMethodDriver(const ProblemFactory &_A, const BasisFactory &_b, size_t maxSpaceLevel, PreSmootherFactory &_pre_smoother, PostSmootherFactory &_post_smoother, ctype _mu_critical, std::function<ctype(size_t)> _h_L, std::function<ctype(size_t)> _tau_L) : __A(_A), B(_b),
                                                                                                                                                                                                                                                                   preSmoother(_pre_smoother), postSmoother(_post_smoother), J(maxSpaceLevel), mu_critical(_mu_critical), tau_L(_tau_L), h_L(_h_L)
    {
    }
    //d is the start value
    //xk_j the initial guess
    //tau_L is the time steps Length
    // h_L is the discretization length, deps on grid, so it is a function
    //time_i is the time resolution in log scale
    //T is the big time
    void solve(VectorType &xk_j, VectorType b_j, size_t time_i, size_t space_j, ctype T)
    {
        //std::cout << "Solve:" << time_i << " sp_" << space_j << std::endl;
        //std::cout << xk_j.size() << "?? " << b_j.size() << std::endl;
        if (time_i <= 0 || space_j == 0)
        {
            //std::cout << "SOLVING" << std::endl;
            auto A_j = A(time_i, space_j); //get matrix from the appropiate time stepping/space
            //Forward substitution or direct solve whole system?
            Dune::UMFPack<MatrixType> solver(A_j, 0);
            Dune::InverseOperatorResult result;
            solver.apply(xk_j, b_j, result);
            return;
        }
        //std::cout << "tau_L:" << tau_L(time_i) << " h_L:" << h_L(space_j) << "=" << tau_L(time_i) * pow(h_L(space_j), -2.0) << std::endl;
        ctype mu_L = tau_L(time_i) * pow(h_L(space_j), -2.0); //TODO: params
        //std::cout << mu_L << "<" << mu_critical << std::endl;
        //entirely sequential
        //TODO(henrik): what happens if you
        if (mu_L < mu_critical)
        {
            //semi coarsening in time
            //std::cout << "going semi" << std::endl;
            this->semiCoarsening(xk_j, b_j, time_i, space_j, T);
        }
        else
        {
            //full space time coarsening

            //std::cout << "going" << std::endl;
            this->fullCoarsening(xk_j, b_j, time_i, space_j, T);
        }
    }

private:
    MatrixType A(int time_i, int space_j)
    {
        if (store.find(std::make_pair(time_i, space_j)) != store.end())
        {
            //std::cout << "Delivering:" << time_i << std::endl;
            return store.at(std::make_pair(time_i, space_j));
        }
        MatrixType a = __A(time_i, space_j);
        store.insert(std::make_pair(std::make_pair(time_i, space_j), a));
        return a;
    }
     PreSmoother pre(int time_i, int space_j)
    {
        if (pre_store.find(std::make_pair(time_i, space_j)) != pre_store.end())
        {
            //std::cout << "Delivering:" << time_i << std::endl;
            return pre_store.at(std::make_pair(time_i, space_j));
        }
        PreSmoother a = __pre(A(time_i,space_j),time_i, space_j);
        pre_store.insert(std::make_pair(std::make_pair(time_i, space_j), a));
        return a;
    }
     PostSmoother post(int time_i, int space_j)
    {
        if (post_store.find(std::make_pair(time_i, space_j)) != post_store.end())
        {
            //std::cout << "Delivering:" << time_i << std::endl;
            return pre_store.at(std::make_pair(time_i, space_j));
        }
        PostSmoother a = __post(A(time_i,space_j),time_i, space_j);
        post_store.insert(std::make_pair(std::make_pair(time_i, space_j), a));
        return a;
    }
    void fullCoarsening(VectorType &xk_j, VectorType b_j, size_t time_i, size_t space_j, ctype T)
    {
        //std::cout << "Full" << std::endl;
        auto A_j = A(time_i, space_j); //get matrix from the appropiate time stepping/space
        //std::cout << "Full" << A_j.N() << std::endl;
        //auto xk_j1 = xk_j;
        preSmoother.apply(xk_j, b_j, A_j, time_i, space_j);
        VectorType d_j(xk_j.size());
        //std::cout << "A_J" << A_j.M() << " " << A_j.N() << std::endl;
        //std::cout << d_j.size() << std::endl;
        d_j = b_j;
        A_j.mmv(xk_j, d_j); //defect calculation
        auto finer = Basis(B(space_j));
        auto coarser = Basis(B(space_j - 1));
        //CGM R;//coarsening matrix
        VectorType d_j_1; //space is kept constant, time has no impact for pt=0 ==> backward euler
        d_j_1.resize(pow(2, time_i - 1) * coarser.size());
        MatrixType R;
        getSpaceTimeRestriction(finer, coarser, pow(2, time_i), pow(0.5, time_i), pow(0.5, time_i), R);
        //std::cout << "R" << R.M() << " " << R.N() << std::endl;
        //std::cout << d_j.size() << std::endl;
        //std::cout << d_j_1.size() << std::endl;
        R.mv(d_j, d_j_1);
        //TODO(henrik): currently V-Cycle only here
        VectorType x_j_1; //space is kept constant, time has no impact for pt=0==>backward euler
        x_j_1.resize(pow(2, time_i - 1) * coarser.size());
        //std::cout << "diff:" << x_j_1.two_norm() << std::endl;
        x_j_1 = 0;
        //full time space coarsening
        solve(x_j_1, d_j_1, time_i - 1, space_j - 1, T); //TODO(henrik): add to timestep extratime
        VectorType y_j;
        y_j.resize(pow(2, time_i) * finer.size());
        R.mtv(x_j_1, y_j);
        xk_j += y_j; //coarse grid correction;
                     //Dune::printvector(datei, xk_j1, "xk_j1", "", 1, 24, 4);
                     //TODO(henrik): avoid copys
        postSmoother.apply(xk_j, b_j, A_j, time_i, space_j);
    }
    //Coarsens in time
    void semiCoarsening(VectorType &xk_j, VectorType b_j, size_t time_i, size_t space_j, ctype T)
    {
        auto finer = Basis(B(space_j));
        //std::cout << "Semi" << std::endl;
        auto A_j = A(time_i, space_j); //get matrix from the appropiate time stepping/space
        //auto xk_j1 = xk_j;
        preSmoother.apply(xk_j, b_j, A_j, time_i, space_j);
        VectorType d_j;
        d_j.resize(pow(2, time_i) * finer.size());
        d_j = b_j;
        A_j.mmv(xk_j, d_j); //defect calculation
        //CGM R;//coarsening matrix
        VectorType d_j_1; //space is kept constant, time has no impact for pt=0 ==> backward euler
        d_j_1.resize(pow(2, time_i - 1) * finer.size());
        //return time coarsening
        //Nt=1 => M_{tau_L}\tilde{M}^{1,2}_{tau_L}=[1]//because pt =0, so they are constant
        //=>
        MatrixType R;

        getTimeRestriction(finer, pow(2, time_i), pow(0.5, time_i), pow(0.5, time_i), R);
        R.mv(d_j, d_j_1); // restriction#
        //std::cout << "restricted" << std::endl;
        //TODO(henrik): currently V-Cycle only here
        VectorType x_j_1; //space is kept constant, time has no impact for pt=0==>backward euler
        //std::cout << time_i << ": " << pow(2, time_i - 1) * finer.size() << std::endl;
        x_j_1.resize(pow(2, time_i - 1) * finer.size());
        //std::cout << "calling solve" << std::endl;
        //only time coarsening
        solve(x_j_1, d_j_1, time_i - 1, space_j, T); //TODO(henrik): add to timestep extratime
        VectorType y_j;
        y_j.resize(pow(2, time_i) * finer.size());
        R.mtv(x_j_1, y_j);
        xk_j += y_j; //coarse grid correction;
                     //Dune::printvector(datei, xk_j1, "xk_j1", "", 1, 24, 4);
                     //TODO(henrik): avoid copys
        postSmoother.apply(xk_j, b_j, A_j, time_i, space_j);
    }
};
#endif
