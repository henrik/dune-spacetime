#ifndef LOADERS_HH
#define LOADERS_HH
#include <string>
#include <fstream>
#include <iomanip>
template<class VectorType>
void writeToFile(VectorType &vector,std::string filename)
{
    std::ofstream datei(filename);
    datei<<vector.size()<<std::endl;
    for(size_t i=0;i<vector.size();i++){
        datei<<  std::setprecision(53)<<vector[i]<<std::endl;
    }
    datei.close();
}
template <class VectorType>
void loadFromFile(VectorType &vector, std::string filename)
{
    std::ifstream datei(filename);
    size_t size;
    datei>>size;
    vector.resize(size);
    double line;
    size_t i =0;
    while(datei>>line){
        vector[i++]=line;
    }
}
#endif