//This follows the tutorial by Oliver Sander (see Introduction to DUNE)
#include "config.h"
#include <dune/common/parallel/mpihelper.hh>

#define HAVE_UG 1
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/matrix.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/yaspgrid.hh>
#include "multigrid.hh"
#include "helpers.hh"
#include "loaders.hh"
#include <dune/functions/functionspacebases/boundarydofs.hh>

#include <dune/istl/umfpack.hh>
#include <dune/common/parametertreeparser.hh>
typedef Dune::BlockVector<Dune::FieldVector<double, 1>> VectorType;
typedef Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>> MatrixType;


template <class Basis>
void getOccupationPattern(const Basis &basis, Dune::MatrixIndexSet &nb,Dune::MatrixIndexSet &nb_B)
{
    nb.resize(basis.size() , basis.size() );
    nb_B.resize(basis.size() , basis.size() );
    auto gridView = basis.gridView();
    auto localView = basis.localView();
    //auto localIndexSet = basis.makeIndexSet();
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        for (size_t i = 0; i < localView.size(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < localView.size(); j++)
            {
                auto col = localView.index(j);
                        nb.add(row, col);
                        nb_B.add(row, col);
            }
        }
    }
}
template <class LocalView, class MType>
void assembleElementMatrix(const LocalView &localView, MType &elementMatrix, MType &elementMassMatrix)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    auto geometry = element.geometry();
    // Get set of shape functions for this element
    const auto &localFiniteElement = localView.tree().finiteElement();
    elementMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    elementMassMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    //init
    elementMatrix = 0;
    elementMassMatrix = 0;
    //calc order
    int order = 2 * (dim * localFiniteElement.localBasis().order() - 1);
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const auto quadPos = quadPoint.position();
        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = geometry.jacobianInverseTransposed(quadPos);
        const auto jacobianNon = geometry.jacobianTransposed(quadPos);
        // The multiplicative factor in the integral transformation formula
        const auto integrationElement = geometry.integrationElement(quadPos);
        // The gradients of the shape functions on the reference element
        std ::vector<Dune::FieldMatrix<double, 1, dim>> referenceGradients;
        std ::vector<Dune::FieldVector<double, 1>> values;
        localFiniteElement.localBasis().evaluateJacobian(quadPos, referenceGradients);
        localFiniteElement.localBasis().evaluateFunction(quadPos, values);
        // Compute the shape function gradients on the real element
        std ::vector<Dune::FieldVector<double, dim>> gradients(referenceGradients.size());
        for (size_t i = 0; i < gradients.size(); i++)
            jacobian.mv(referenceGradients[i][0], gradients[i]);
        /*std::cout << integrationElement << std::endl;
        std::cout << "size:" << values.size() << std::endl;
        for (const auto &val : values)
            std::cout << val << std::endl;
       // std::cout << "quad:" << quadPoint.weight() << std::endl;*/
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                //normally we would now add code for kroneckerproduct.

                //Added second part for matrix
                elementMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += ((gradients[i] * gradients[j]) * quadPoint.weight() * integrationElement) + values[i] * values[j] * quadPoint.weight() * integrationElement;
                //Added second part for matrix
                elementMassMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += values[i] * values[j] * quadPoint.weight() * integrationElement;
            }
        }
    }
}
// Compute the heatsource term for a single element
template <class LocalView>
void getStartHeat(const LocalView &localView,
                  Dune::BlockVector<Dune::FieldVector<double, 1>> &localRhs,
                  const std::function<double(Dune::FieldVector<double, LocalView::Element::dimension>)> heatTerm)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    // Set of shape functions for a single element
    const auto &localFiniteElement = localView.tree().finiteElement();
    // Set all entries to zero
    localRhs.resize(localFiniteElement.size());
    localRhs = 0;
    // A quadrature rule
    int order = dim;
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    // Loop over all quadrature points
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const Dune::FieldVector<double, dim> &quadPos = quadPoint.position();
        // The multiplicativefactor in the integraltransformation formula
        const double integrationElement = element.geometry().integrationElement(quadPos);
        double functionValue = heatTerm(element.geometry().global(quadPos));
        // Evaluate all shape function values at this point
        std ::vector<Dune::FieldVector<double, 1>> shapeFunctionValues;
        localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);
        // Actually compute the vector entries
        for (size_t i = 0; i < localRhs.size(); i++)
            localRhs[i] += shapeFunctionValues[i] * functionValue * quadPoint.weight();
    }
}
template <class Basis>
void assembleHeatEquationProblemForMethodOfLinesWithoutInvert(const Basis &basis,
                                                              MatrixType &matrix,
                                                              MatrixType &massMatrix,
                                                              VectorType &rhs,
                                                              const std::function<double(Dune::FieldVector<double, Basis::GridView::dimension>)> heatTerm,
                                                              const double timestepSize)
{
    auto gridView = basis.gridView();
    //get Occupation pattern
    Dune::MatrixIndexSet occupationPattern,occPatternMass;
    getOccupationPattern(basis, occupationPattern,occPatternMass );
    //and apply it so we can access the entries
    occupationPattern.exportIdx(matrix);
    occPatternMass.exportIdx(massMatrix);
    //occupationPattern.exportIdx(massMatrix);
    matrix = 0;
    //massMatrix = 0;
    // set rhs to correct length
    rhs.resize(basis.size());
    //set all rhs to zero
    rhs = 0;
    auto localView = basis.localView();
    // Mark all dirichletNodes
    std ::vector<char> dirichletNodes(basis.size(), false);
    Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
        dirichletNodes[index] = true;
    });
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        // Now get the localcontribution to the right −hand side vector
        Dune::BlockVector<Dune::FieldVector<double, 1>> localRhs;
        getStartHeat(localView, localRhs, heatTerm);
        for (size_t i = 0; i < localRhs.size(); i++)
        {
            // The global index of the i −th vertex
            auto row = localView.index(i);
            if (dirichletNodes[row] == false)
                rhs[row] += localRhs[i];
        }
    }
}
template<const int dim,class GridType>void  render(bool cube,size_t nOfTimeSteps,double sizeOfTimeSteps,size_t grid_refine,std::string filename,std::string vtk_filename){
     typedef Dune::FieldVector<double, dim> V2;
        V2 ll, ur;
        //ur[1] = 1;
        for(int i=0;i<dim;i++)
        ur[i] = 1;
        std::array<unsigned, dim> ar;
        const size_t W = 2;
        for(int i=0;i<dim;i++)
        ar[i] = W;
        auto grid = cube ?Dune::StructuredGridFactory<GridType>::createCubeGrid(ll, ur, ar):Dune::StructuredGridFactory<GridType>::createSimplexGrid(ll, ur, ar);
        //std ::shared_ptr<GridType> grid(Dune::GmshReader<GridType>::read("l-shape.msh"));
        grid->globalRefine(grid_refine);
        typedef typename GridType::LeafGridView GridView;
        GridView gridView = grid->leafGridView();
        Dune::Functions::LagrangeBasis<GridView, 1> basis(gridView);
        auto sourceTerm = [](const Dune::FieldVector<double, dim> &x) {
            double minus =0.0;
            for (int i =0;i<dim;i++)
                minus += (x[i] - 0.5)*(x[i] - 0.5);
            double res = 0.25- minus;
            if (res<0)
                return 0.0;
            return res;
        };
        VectorType rhs;
        MatrixType matrix,massMatrix;
        assembleHeatEquationProblemForMethodOfLinesWithoutInvert(basis, matrix,
                                                                 massMatrix,
                                                                 rhs, sourceTerm, sizeOfTimeSteps);
        // Mark all dirichletNodes
        //boundary cdt taken from https://github.com/dune-project/dune-functions/blob/master/examples/poisson-pq2.cc
        Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
            //dirichletNodes[index] = true;
            rhs[index] = 0.0;
        });
        if(filename !=""){
            writeToFile(rhs,filename);
        }
        if(vtk_filename !=""){
            Dune::VTKWriter<GridView> vtkWriter(gridView);
            vtkWriter.addVertexData(rhs, "solution");
            vtkWriter.write(vtk_filename);
        }
}
int main(int argc, char **argv)
{
    Dune::MPIHelper::instance(argc, argv);
    try
    {
        Dune::ParameterTree pt;
        Dune::ParameterTreeParser::readOptions(argc,argv,pt);
        const size_t nOfTimeSteps = pt.get<int>("timestepcount",256);
        const double sizeOfTimeSteps = 1.0/((double)pt.get<int>("timestepcount",256));
        if(pt.hasKey("threed")){
        const int dim = 3;
        if(pt.hasKey("cube")){
            typedef Dune::YaspGrid<dim> GridType;
            render<dim,GridType> (true,nOfTimeSteps,sizeOfTimeSteps,pt.get<int>("refine",3),pt.get<std::string>("o",""),pt.get<std::string>("vtk",""));
        } else {
            typedef Dune::UGGrid<dim> GridType;
            render<dim,GridType> (false,nOfTimeSteps,sizeOfTimeSteps,pt.get<int>("refine",3),pt.get<std::string>("o",""),pt.get<std::string>("vtk",""));
        }
        }
        if(pt.hasKey("twod")){
        const int dim = 2;
        if(pt.hasKey("cube")){
            typedef Dune::YaspGrid<dim> GridType;
            render<dim,GridType> (true,nOfTimeSteps,sizeOfTimeSteps,pt.get<int>("refine",3),pt.get<std::string>("o",""),pt.get<std::string>("vtk",""));
        } else {
            typedef Dune::UGGrid<dim> GridType;
            render<dim,GridType> (false,nOfTimeSteps,sizeOfTimeSteps,pt.get<int>("refine",3),pt.get<std::string>("o",""),pt.get<std::string>("vtk",""));
        }
        }
        if(pt.hasKey("oned")){
        const int dim = 1;
        if(pt.hasKey("cube")){
            typedef Dune::YaspGrid<dim> GridType;
            render<dim,GridType> (true,nOfTimeSteps,sizeOfTimeSteps,pt.get<int>("refine",3),pt.get<std::string>("o",""),pt.get<std::string>("vtk",""));
        } else {
            ///render<dim,GridType> (false,nOfTimeSteps,sizeOfTimeSteps,pt.get<int>("refine",3),pt.get<std::string>("o",""),pt.get<std::string>("vtk",""));
        }
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
}