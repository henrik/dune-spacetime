#include "config.h"
#include <dune/common/parallel/mpihelper.hh>
#include <dune/istl/bvector.hh>
#include "loaders.hh"
#include <dune/common/parametertreeparser.hh>
typedef Dune::BlockVector<Dune::FieldVector<double, 1>> VectorType;

int main(int argc, char **argv)
{
    Dune::MPIHelper::instance(argc, argv);
    try
    {
        Dune::ParameterTree pt;
        Dune::ParameterTreeParser::readOptions(argc, argv,pt);
        VectorType a,b;
        loadFromFile(a,pt.get("a","a.test"));
        loadFromFile(b,pt.get("b","b.test"));
        if(pt.get<int>("full",0)!=0){
        std::cout<<a.two_norm()<<std::endl;
        } else {
            a-= b;
        std::cout<<a.two_norm()<<std::endl;
        }
    } catch(Dune::Exception &e){

    }
}