
#include <dune/common/fvector.hh>
#include <dune/geometry/type.hh>
#include "helpers.hh"
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

/**
   This function may be used by the VTKWriter object to evaluate the
   functor object at a given local coordinate.
*/
template <class Grid, class Functor>
class FunctorVTKFunction : public Dune::VTKFunction<Grid>
{
private:
  // Some types which are exported by the base class
  typedef Dune::VTKFunction<Grid> Base;
  typedef typename Base::Entity Entity;
  typedef typename Base::ctype ctype;
  enum
  {
    dim = Base::dim
  };
  // Constant reference to the functor object.
  const Functor &f;
  const int width;
  const std::string data_name;

public:
  FunctorVTKFunction(const Functor &f_, std::string name_, int width_) : f(f_), data_name(name_), width(width_)
  {
  }

  //! The number of components of this function. This is necessary as
  //! the VTKWriter would also allow the visualization of vector
  //! valued functions.
  int ncomps() const { return 1; }
  //! Evaluate the functor at a given local coordinate
  double evaluate(int comp, const Entity &e, const Dune::FieldVector<ctype, dim> &xi) const
  {
    auto x_global = e.geometry().corner(0);
    bool upside = e.geometry().corner(0)[1] > e.geometry().corner(1)[1];
    int x = (x_global[0]) * (width);
    int y = (upside ? e.geometry().corner(1)[1] : x_global[1]) * (width);
    /*std::cout<<" "
     <<e.geometry().corner(0)<<" "
     <<e.geometry().corner(1)<< " "
     <<e.geometry().corner(2)
     <<std::endl<<e.geometry().global(xi)<<std::endl;*/
    double val = 0;
    //bool left_corner = e.geometry().corner(0)==e.geometry().global(xi);// means where are one or two
    int i = 2;
    if (xi[1] == 0)
    {
      if (xi[0] == 0)
      {
        i = 0;
      }
      else
      {
        i = 1;
      }
    }
    //std::cout<<i<<std::endl;
    //accessV2D<DV,double>(daten, i, j, true, i, W)
    val = accessV2Dc<Functor, double>(f, x, y, upside, i, width);
    return val;
    //return xi[0]*2 + xi[1];
  }

  //! This name will later be displayed as a label for the visualized
  //! data in paraview.
  std::string name() const
  {
    return data_name;
  }
};

//! Write the VTK file.
template <class GridView, class Functor>
void writeVTKFile(const GridView &gridview, const Functor &f, std::string filename)
{
  Dune::SubsamplingVTKWriter<GridView> vtkwriter(gridview, Dune::refinementLevels(0));
  typedef FunctorVTKFunction<GridView, Functor> VTKFunctor;
  std::cout << "gridview.size(0)" << gridview.size(0) << std::endl;
  auto vtk_f = std::make_shared<VTKFunctor>(f, "Functor", sqrt(gridview.size(0) / 2)+1);
  vtkwriter.addVertexData(vtk_f);
  vtkwriter.write(filename, Dune::VTK::appendedraw);
  std::cout << "Wrote VTK file" << std::endl;
}