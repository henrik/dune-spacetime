#include <dune/pdelab.hh>
#include "prolongation.hh"
typedef Dune::BlockVector<Dune::FieldVector<double, 1>> VectorType;
typedef Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>> MatrixType;
template <class Basis>
void getOccupationPattern(const Basis &basis, Dune::MatrixIndexSet &nb)
{
    nb.resize(basis.size(), basis.size());
    auto gridView = basis.gridView();
    auto localView = basis.localView();
    //auto localIndexSet = basis.makeIndexSet();
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        for (size_t i = 0; i < localView.size(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < localView.size(); j++)
            {
                auto col = localView.index(j);
                nb.add(row, col);
            }
        }
    }
}
template <class LocalView, class MType>
void assembleElementMatrix(const LocalView &localView, MType &elementMatrix, MType &elementMassMatrix, double h)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    auto geometry = element.geometry();
    // Get set of shape functions for this element
    const auto &localFiniteElement = localView.tree().finiteElement();
    elementMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    elementMassMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    //init
    elementMatrix = 0;
    elementMassMatrix = 0;
    //calc order
    int order = 2 * (dim * localFiniteElement.localBasis().order() - 1);
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const auto quadPos = quadPoint.position();
        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = geometry.jacobianInverseTransposed(quadPos);
        const auto jacobianNon = geometry.jacobianTransposed(quadPos);
        // The multiplicative factor in the integral transformation formula
        const auto integrationElement = geometry.integrationElement(quadPos);
        // The gradients of the shape functions on the reference element
        std ::vector<Dune::FieldMatrix<double, 1, dim>> referenceGradients;
        std ::vector<Dune::FieldVector<double, 1>> values;
        localFiniteElement.localBasis().evaluateJacobian(quadPos, referenceGradients);
        localFiniteElement.localBasis().evaluateFunction(quadPos, values);
        // Compute the shape function gradients on the real element
        std ::vector<Dune::FieldVector<double, dim>> gradients(referenceGradients.size());
        for (size_t i = 0; i < gradients.size(); i++)
            jacobian.mv(referenceGradients[i][0], gradients[i]);
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                //Added second part for matrix
                elementMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += ((gradients[i] * gradients[j]) * quadPoint.weight() * integrationElement) + values[i] * values[j] * quadPoint.weight() * integrationElement * 1.0 / h;
                //Added second part for matrix
                elementMassMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += values[i] * values[j] * quadPoint.weight() * integrationElement * 1.0 / h;
            }
        }
    }
}
// Compute the heatsource term for a single element
template <class LocalView>
void getStartHeat(const LocalView &localView,
                  Dune::BlockVector<Dune::FieldVector<double, 1>> &localRhs,
                  const std::function<double(Dune::FieldVector<double, LocalView::Element::dimension>)> heatTerm)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    // Set of shape functions for a single element
    const auto &localFiniteElement = localView.tree().finiteElement();
    // Set all entries to zero
    localRhs.resize(localFiniteElement.size());
    localRhs = 0;
    // A quadrature rule
    int order = dim;
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    // Loop over all quadrature points
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const Dune::FieldVector<double, dim> &quadPos = quadPoint.position();
        // The multiplicativefactor in the integraltransformation formula
        const double integrationElement = element.geometry().integrationElement(quadPos);
        double functionValue = heatTerm(element.geometry().global(quadPos));
        // Evaluate all shape function values at this point
        std ::vector<Dune::FieldVector<double, 1>> shapeFunctionValues;
        localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);
        // Actually compute the vector entries
        for (size_t i = 0; i < localRhs.size(); i++)
            localRhs[i] += shapeFunctionValues[i] * functionValue * quadPoint.weight();
    }
}
template <class Basis>
void assembleHeatEquationProblemForMethodOfLinesWithoutInvert(const Basis &basis,
                                                              MatrixType &matrix,
                                                              MatrixType &massMatrix,
                                                              VectorType &rhs,
                                                              const std::function<double(Dune::FieldVector<double, Basis::GridView::dimension>)> heatTerm,
                                                              double h)
{
    auto gridView = basis.gridView();
    //get Occupation pattern
    Dune::MatrixIndexSet occupationPattern;
    getOccupationPattern(basis, occupationPattern);
    //and apply it so we can access the entries
    occupationPattern.exportIdx(matrix);
    occupationPattern.exportIdx(massMatrix);
    matrix = 0;
    massMatrix = 0;
    // set rhs to correct length
    rhs.resize(basis.size());
    //set all rhs to zero
    rhs = 0;
    auto localView = basis.localView();
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        Dune::Matrix<Dune::FieldMatrix<double, 1, 1>> elementMatrix, elementMassMatrix;
        assembleElementMatrix(localView, elementMatrix, elementMassMatrix, h);
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                auto col = localView.index(j);
                matrix[row][col] += elementMatrix[i][j];
                massMatrix[row][col] += elementMassMatrix[i][j];
            }
        }
        // Now get the localcontribution to the right −hand side vector
        Dune::BlockVector<Dune::FieldVector<double, 1>> localRhs;
        getStartHeat(localView, localRhs, heatTerm);
        for (size_t i = 0; i < localRhs.size(); i++)
        {
            // The global index of the i −th vertex
            auto row = localView.index(i);
            rhs[row] += localRhs[i];
        }
    }
    // Mark all dirichletNodes
    std ::vector<char> dirichletNodes(basis.size(), false);
    //boundary cdt taken from https://github.com/dune-project/dune-functions/blob/master/examples/poisson-pq2.cc
    Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
        dirichletNodes[index] = true;
    });
    for (size_t i = 0; i < matrix.N(); i++)
    {
        if (dirichletNodes[i])
        {
            rhs[i] = 0.0; //rhs should have zero bd condition
            {
                auto cIt = matrix[i].begin();
                auto cEndIt = matrix[i].end();
                // loop over nonzero matrix entries in current row
                for (; cIt != cEndIt; ++cIt)
                    *cIt = (i == cIt.index()) ? 1 : 0;
            }
            {
                auto cIt = massMatrix[i].begin();
                auto cEndIt = massMatrix[i].end();
                // loop over nonzero matrix entries in current row
                for (; cIt != cEndIt; ++cIt)
                    *cIt = (i == cIt.index()) ? 1 : 0;
            }
        }
        else
        {
            {
                auto cIt = matrix[i].begin();
                auto cEndIt = matrix[i].end();
                // loop over nonzero matrix entries in current row
                for (; cIt != cEndIt; ++cIt)
                    if (dirichletNodes[cIt.index()])
                        *cIt = 0;
            }
            {
                auto cIt = massMatrix[i].begin();
                auto cEndIt = massMatrix[i].end();
                // loop over nonzero matrix entries in current row
                for (; cIt != cEndIt; ++cIt)
                    if (dirichletNodes[cIt.index()])
                        *cIt = 0;
            }
        }
    }
}

/** Manage a hierarchy of assemblers
 */
template<typename FS,typename LOP,Dune::SolverCategory::Category st = Dune::SolverCategory::sequential>
class HeatEquationProblemAssemblerHierarchy
{
public:
  // export types
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  /*typedef Dune::PDELab::GridOperator<typename FS::GFS,typename FS::GFS,LOP,MBE,
                                     typename FS::NT,typename FS::NT,typename FS::NT,
                                     typename FS::CC,typename FS::CC> GO;
  */
    typedef Dune::PDELab::GridOperator<typename FS::GFS,typename FS::GFS,LOP,MBE,
                                     typename FS::NT,typename FS::NT,typename FS::NT,
                                     typename FS::CC,typename FS::CC> GO;
  typedef typename GO::Jacobian MAT;
  typedef VectorHierarchy<FS> VH;
  typedef MatrixHierarchy<HeatEquationProblemAssemblerHierarchy<FS,LOP,st> > MH;

  HeatEquationProblemAssemblerHierarchy (const FS& fs,LOP& lop,const std::size_t nonzeros) : gop(fs.maxLevel()+1)
  {
    for (int j=0; j<=fs.maxLevel(); j++)//TODO: update generation
      gop[j] = std::make_shared<GO>(fs.getGFS(j),fs.getCC(j),fs.getGFS(j),fs.getCC(j),lop,MBE(nonzeros));
  }

  int maxLevel () const
  {
    return gop.size()-1;
  }

  // return grid reference
  GO& getGO (int j)
  {
    if (j>=0 && j<gop.size())
      return *gop[j];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // return grid reference const version
  const GO& getGO (int j) const
  {
    if (j>=0 && j<gop.size())
      return *gop[j];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // evaluate jacobian on all levels
  void jacobian (const VH& xh, MH& mh)
  {
    for (int j=0; j<=maxLevel(); j++)
      {
        mh[j] = 0.0;
        gop[j]->jacobian(xh[j],mh[j]);
      }
  }

private:
  std::vector<std::shared_ptr<GO> > gop;
};