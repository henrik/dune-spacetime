#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
//IV: Initial value
//LV: Load value
//SM: StiffnessMatrix
//CGM: Coarse Grid correction Matrix
template <typename IV, typename LV, typename SM, typename CGM,typename ctype>
IV MGM(size_t j, IV xk_j, LV b_j, std::function<SM(size_t)> A_factory, std::function<CGM(size_t)> R_factory, std::function<ctype(SM,size_t)> omega_factory, size_t nu_1, size_t nu_2, size_t solve_level=2) //TODO(henrik): omega übergeben
{
    SM A_j = A_factory(j);

    std::cout << "A_" << j << ":" << A_j.M() << "x" << A_j.N() << std::endl;
    //std::ofstream datei("debug_File.txt");
    auto omega = omega_factory(A_j, j); //Omega < 2/lambda_max
    if (j <= solve_level)
    {
        std::cout << "SOLVING" << std::endl;

        // Solve linear system taken from sheet 02 FEM 201920
        typedef Dune::MatrixAdapter<SM, LV, LV> LinearOperator;
        LinearOperator op(A_j);
        typedef Dune::SeqILU<SM, LV, LV> Preconditioner;
        Preconditioner prec(A_j, 1.8);

        const ctype reduction = 1e-9;
        const int max_iterations = 5000;
        const int verbose = 1;

        typedef Dune::CGSolver<LV> Solver;
        Solver solver(op, prec, reduction, max_iterations, verbose);
        Dune::InverseOperatorResult result;

       // Dune::printvector(datei, b_j, "b_j", "", 1, 24, 4);
        solver.apply(xk_j, b_j, result);
        // A_j.solve(xk_j,b_j,false);
        //Dune::printvector(datei, xk_j, "xk_j", "", 1, 24, 4);
        return xk_j;
    }
    auto R = R_factory(j);
    //Dune::DynamicVector<double> d;
    std::cout<<xk_j.dim()<<std::endl;
    std::cout << "R_j:" << R.M() << "x" << R.N() << std::endl;
    IV xk_j1 = xk_j;
    for (size_t k = 0; k < nu_1; k++)
    {
        IV def = b_j; //Be sure to copy here
        A_j.mmv(xk_j1, def);
        def *= omega;
        xk_j1 += (def); //richardson iteration pre smoothing
    }
    IV d_j = b_j;
    //Dune::printvector(datei, b_j, "b_j", "", 1, 24, 4);
    A_j.mmv(xk_j1, d_j); //defect calculation
    //CGM R;//coarsening matrix
    //Dune::printvector(datei, d_j, "d_j", "", 1, 24, 4);
    IV d_j_1(R.N());
    R.mv(d_j, d_j_1); // restriction
    //Dune::printvector(datei, d_j_1, "d_j_1", "", 1, 24, 4);
    d_j_1 *= 0.4;
    //TODO(henrik): currently V-Cycle only here
    LV zero(R.N());
    IV x_j_1 = MGM<IV, LV, SM, CGM>(j - 1, zero, d_j_1, A_factory, R_factory, omega_factory, nu_1, nu_2);
    //std::cout << "E" << std::endl;
    IV y_j(R.M());

    R.mtv(x_j_1, y_j); //Prolongation, QUESTION(Multiply by factor in order to reset restriction)
    xk_j1 += y_j;      //coarse grid correction;
                       //Dune::printvector(datei, xk_j1, "xk_j1", "", 1, 24, 4);
    for (size_t k = 0; k < nu_2; k++)
    {
        IV def = b_j;
        A_j.mmv(xk_j1, def);
        def *= omega;
        xk_j1 += (def); //richardson iteration pre smoothing
    }
    //Dune::printvector(datei, xk_j1, "xk_j(smooth)", "", 1, 24, 4);
    return xk_j1;
}