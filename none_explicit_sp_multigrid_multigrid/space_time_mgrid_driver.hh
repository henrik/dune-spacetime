#ifndef SPACE_TIME_MGRID_DRIVER_HH
#define SPACE_TIME_MGRID_DRIVER_HH
#define USE_TBB_HERE
#include <dune/istl/umfpack.hh>
#include "prolongation.hh"
#include <tbb/parallel_for.h>
//the structure of this class is based on dune--parsolve MultigridOnOverlappinggrids
template <class Basis,class GridType, class MatrixType, class VectorType, class PreSmoother, class PostSmoother, typename ctype>
class SpaceTimeMultiGridMethodDriver
{

    PreSmoother &preSmoother; //not const, because they might be adaptive or so
    PostSmoother &postSmoother;
    int J;
    ctype mu_critical,T;
    std::function<ctype(size_t)> tau_L;
    std::function<ctype(size_t)> h_L;
    std::map<std::pair<int, int>, MatrixType*> store;
    std::map<std::pair<int, int>, MatrixType*> storeL;
    std::map<std::pair<int, int>, MatrixType*>  storeSmall;
    std::map<std::pair<int, int>, MatrixType*>  storeSmall2;
    std::map<int,MatrixType*> storetimeRestriction;
    std::map<int,MatrixType*> storespaceRestriction;
    std::map<std::pair<int, int>, MatrixType*> storespaceTimeRestriction;
public:
    typedef std::function<MatrixType(int,int, Basis&)> ProblemFactory;
    typedef typename Basis::GridView BGV;
    GridType *B;
    size_t nu_1;
    size_t gamma;
    int solveTimeAtLevel;
    const ProblemFactory &__A;
    const ProblemFactory &__L;
    const ProblemFactory &__Asmall;
    const ProblemFactory &__Asmall2;
    SpaceTimeMultiGridMethodDriver(const ProblemFactory &_A,const ProblemFactory &_L, const ProblemFactory &_Asmall,const ProblemFactory &_Asmall2,  GridType *_b, size_t maxSpaceLevel, PreSmoother &_pre_smoother, PostSmoother &_post_smoother, ctype _mu_critical,  ctype _T , std::function<ctype(size_t)> _h_L, std::function<ctype(size_t)> _tau_L, int nut, int _solveTimeAtLevel, size_t _gamma) : __A(_A),__L(_L),
                                                                                                                                                                                                                                                                                                  __Asmall(_Asmall),
                                                                                                                                                                                                                                                                                                  __Asmall2(_Asmall2),
                                                                                                                                                                                                                                                                                                  gamma(_gamma),
                                                                                                                                                                                                                                                                                                  B(_b),
                                                                                                                                                                                                                                                                                                  T(_T),
                                                                                                                                                                                                                                                                                                  nu_1(nut),
                                                                                                                                                                                                                                                                                                  solveTimeAtLevel(_solveTimeAtLevel),
                                                                                                                                                                                                                                                                                                  preSmoother(_pre_smoother), postSmoother(_post_smoother), J(maxSpaceLevel), mu_critical(_mu_critical), tau_L(_tau_L), h_L(_h_L)
    {
    }
    //d is the start value
    //xk_j the initial guess
    //tau_L is the time steps Length
    // h_L is the discretization length, deps on grid, so it is a function
    //time_i is the time resolution in log scale
    void solve(VectorType &xk_j, VectorType b_j, size_t time_i, size_t space_j)
    {
        ctype mu_L = tau_L(time_i) * pow(h_L(space_j), -2.0); //TODO: non uniform grid
          if (mu_L < mu_critical)
        {
            //semi coarsening in time
            if(time_i>solveTimeAtLevel){
            this->semiCoarsening(xk_j, b_j, time_i, space_j);
            return ;
            }
        }
            //full space time coarsening
        if(time_i>solveTimeAtLevel&& space_j>0){
            this->fullCoarsening(xk_j, b_j, time_i, space_j);
            return;
        }
        //semi coarsening in time, if possible.
        if(time_i>solveTimeAtLevel){
            this->semiCoarsening(xk_j, b_j, time_i, space_j);
            return ;
        }
        if (time_i <= solveTimeAtLevel|| space_j == 0)
        {
            auto A_j = A(time_i, space_j); //get matrix from the appropiate time stepping/space
            Dune::UMFPack<MatrixType> solver(*A_j, 0);
            Dune::InverseOperatorResult result;
            solver.apply(xk_j, b_j, result);
            return;
        }
    }

private:
    MatrixType* A(int time_i, int space_j)
    {
        if (store.find(std::make_pair(time_i, space_j)) != store.end())
        {
            return store.at(std::make_pair(time_i, space_j));
        }
        auto b = Basis(B->levelGridView(space_j));
        MatrixType* a =new MatrixType( __A(time_i,space_j, b));
        store.insert(std::make_pair(std::make_pair(time_i, space_j), a));
        return a;
    }
    //Lower Matrix
    MatrixType* L(int time_i, int space_j)
    {
        if (storeL.find(std::make_pair(time_i, space_j)) != store.end())
        {
            return store.at(std::make_pair(time_i, space_j));
        }
        auto b = Basis(B->levelGridView(space_j));
        MatrixType* a = new MatrixType(__L(time_i,space_j, b));
        store.insert(std::make_pair(std::make_pair(time_i, space_j), a));
        return a;
    }

    MatrixType* Asmall( int space_j,int time_i)
    {
        if (storeSmall.find(std::make_pair(time_i, space_j)) != storeSmall.end())
        {
            return storeSmall.at(std::make_pair(time_i, space_j));
        }
        auto b = Basis(B->levelGridView(space_j));
        MatrixType* a = new MatrixType(__Asmall(time_i,space_j, b));
        storeSmall.insert(std::make_pair(std::make_pair(time_i, space_j), a));
        return a;
    }
    MatrixType* Asmall2( int space_j,int time_i)
    {
        if (storeSmall2.find(std::make_pair(time_i, space_j)) != storeSmall2.end())
        {
            return storeSmall2.at(std::make_pair(time_i, space_j));
        }
        auto b = Basis(B->levelGridView(space_j));
        MatrixType* a = new MatrixType(__Asmall2(time_i,space_j, b));
        storeSmall2.insert(std::make_pair(std::make_pair(time_i, space_j), a));
        return a;
    }
    void fullCoarsening(VectorType &xk_j, VectorType b_j, size_t time_i, size_t space_j)
    {
        auto A_j = A(time_i, space_j); //get matrix from the appropiate time stepping/space
        preSmootherapply(xk_j, b_j, time_i, space_j);
        VectorType d_j(xk_j.size());
        d_j = b_j;
        A_j->mmv(xk_j, d_j); //defect calculation
        auto finer = Basis(B->levelGridView(space_j));
        auto coarser = Basis(B->levelGridView(space_j - 1));
        VectorType d_j_1; 
        d_j_1.resize(pow(2, time_i - 1) * coarser.size());
        MatrixType* R = spaceTimeRestriction(time_i,space_j,finer, coarser);
        R->mv(d_j, d_j_1);
        VectorType x_j_1;
        x_j_1.resize(pow(2, time_i - 1) * coarser.size()); //resize to new size
        x_j_1 = 0;
        
        size_t gamma_bar =1;
        if (time_i!=1){ //W cycle  possible
            gamma_bar= gamma;
        }
        //full time space coarsenings
        for(int i=0;i<gamma_bar;i++)
        solve(x_j_1, d_j_1, time_i - 1, space_j - 1);
        VectorType y_j;
        y_j.resize(pow(2, time_i) * finer.size());
        R->mtv(x_j_1, y_j);
        xk_j += y_j;
        postSmootherapply(xk_j, b_j, time_i, space_j);
    }
    //Coarsens only in time
    void semiCoarsening(VectorType &xk_j, VectorType b_j, size_t time_i, size_t space_j)
    {
        auto finer = Basis(B->levelGridView(space_j));
        auto A_j = A(time_i, space_j); //get matrix from the appropiate time stepping/space
        preSmootherapply(xk_j, b_j, time_i, space_j);
        VectorType d_j;
        d_j.resize(pow(2, time_i) * finer.size());
        d_j = b_j;
        A_j->mmv(xk_j, d_j); //defect calculation
        VectorType d_j_1;
        d_j_1.resize(pow(2, time_i - 1) * finer.size());
        MatrixType* R= timeRestriction(time_i,finer);
        R->mv(d_j, d_j_1); // restriction
        VectorType x_j_1;
        x_j_1.resize(pow(2, time_i - 1) * finer.size());
        //only time coarsening
        size_t gamma_bar =1;
        if (time_i!=1){
           gamma_bar= gamma;
        }
        for(int k=0;k<gamma_bar;k++)
        solve(x_j_1, d_j_1, time_i - 1, space_j);
        VectorType y_j;
        y_j.resize(pow(2, time_i) * finer.size());
        R->mtv(x_j_1, y_j);
        xk_j += y_j;
        postSmootherapply(xk_j, b_j, time_i, space_j);
    }
    void postSmootherapply(VectorType &xk_j, VectorType &b_j, size_t time_i, size_t space_j)
    {
        for(int i =0;i<nu_1;i++)
        jacobiSmoother(xk_j, b_j, time_i, space_j);
    }
    void preSmootherapply(VectorType &xk_j, VectorType &b_j, size_t time_i, size_t space_j)
    {
       for(int i =0;i<nu_1;i++)
        jacobiSmoother(xk_j, b_j, time_i, space_j);
    }
    MatrixType* spaceRestriction(size_t space_j, Basis & finer, Basis &coarser){
        if (storespaceRestriction.find( space_j) != storespaceRestriction.end())
        {
            return storespaceRestriction.at( space_j);
        }
        MatrixType R;
        getSpaceRestriction(finer,coarser,R);
        MatrixType *RP = new MatrixType(R);
        storespaceRestriction.insert(std::make_pair(space_j, RP));
        return RP;
    }
    MatrixType* timeRestriction(size_t time_i, Basis & basis){
        if (storetimeRestriction.find( time_i) != storetimeRestriction.end())
        {
            return storetimeRestriction.at( time_i);
        }
        MatrixType R;
        getTimeRestriction(basis, pow(2, time_i), T*pow(0.5, time_i), T*pow(0.5, time_i), R);
        MatrixType *RP = new MatrixType(R);
        storetimeRestriction.insert(std::make_pair(time_i, RP));
        return RP;
    }
    MatrixType* spaceTimeRestriction(size_t time_i, size_t space_j, Basis & finer, Basis &coarser){
        if (storespaceTimeRestriction.find( std::make_pair(time_i,space_j)) != storespaceTimeRestriction.end())
        {
            return storespaceTimeRestriction.at( std::make_pair(time_i,space_j));
        }
        MatrixType R;
        getSpaceTimeRestriction(finer, coarser, pow(2, time_i), T*pow(0.5, time_i), T*pow(0.5, time_i), R);
        MatrixType *RP = new MatrixType(R);
        storespaceTimeRestriction.insert(std::make_pair(std::make_pair(time_i,space_j), RP));
        return RP;
    }
    //spatial multigrid
    void msolve(VectorType &xk_j,  VectorType & b_j, size_t space_j,size_t time_i,bool diagonal, bool customSmooth)
    {
       auto A_j = diagonal ? Asmall( space_j,time_i):Asmall2(space_j,time_i); //get matrix from the appropiate time stepping/space
        if(space_j<=0){
            Dune::UMFPack<MatrixType> solver(*A_j, 0);
            Dune::InverseOperatorResult result;
            solver.apply(xk_j, b_j, result);
            return;
        }
        auto finer = Basis(B->levelGridView(space_j));
        auto coarser = Basis(B->levelGridView(space_j-1));
        preSmoother.apply(xk_j, b_j,A_j, space_j);
        VectorType d_j;
        d_j.resize(finer.size());
        d_j = b_j;
        A_j->mmv(xk_j, d_j); //defect calculation
        VectorType d_j_1; 
        d_j_1.resize(coarser.size());
        MatrixType* R =spaceRestriction(space_j,finer, coarser);
        R->mv(d_j, d_j_1); // restriction#
        VectorType x_j_1;
        x_j_1.resize(coarser.size());
        //TODO(henrik): currently V-Cycle only here
        msolve(x_j_1, d_j_1, space_j -1,time_i,diagonal, customSmooth);
        VectorType y_j;
        y_j.resize( finer.size());
        R->mtv(x_j_1, y_j);
        xk_j += y_j;
        postSmoother.apply(xk_j, b_j, A_j, space_j);
    }
    //IMPLEMENTING according to rannacher num0
    void jacobiSmoother(VectorType &xk_j,  VectorType b_j, size_t time_i, size_t space_j)
    {
        //x^t=D^{-1}(b-(L+R)x^{t-1})
        //R=0
        VectorType xk_j2;
        xk_j2.resize(xk_j.size());
        MatrixType* _L = L(time_i,space_j);
        auto finer = Basis(B->levelGridView(space_j));
        _L->mmv(xk_j,b_j);
        {
            VectorType x_tau,b_j_tau;
            x_tau.resize(finer.size());
            b_j_tau.resize(finer.size());
            std::copy(b_j.begin(),b_j.begin()+b_j_tau.size(),b_j_tau.begin());
            msolve(x_tau, b_j_tau, space_j,time_i,false,false);
            //damped
            x_tau *= 0.5;
            std::copy(x_tau.begin(),x_tau.end(),xk_j2.begin());
        }
#ifdef USE_TBB_HERE
        tbb::parallel_for(1,(int)pow(2,time_i),[&](int i){
#else
        for(int i=1;i< pow(2,time_i);i++){
#endif
            VectorType x_tau,b_j_tau;
            x_tau.resize(finer.size());
            b_j_tau.resize(finer.size());
            std::copy(b_j.begin()+i*b_j_tau.size(), b_j.begin()+(i+1)*b_j_tau.size(), b_j_tau.begin());
            msolve(x_tau, b_j_tau, space_j,time_i,false,false);
            //damped
            x_tau *= 0.5;
            std::copy(x_tau.begin(),x_tau.end(),xk_j2.begin()+i*x_tau.size());
 #ifdef USE_TBB_HERE
        });
#else
        }
#endif
        xk_j += xk_j2;
    }
};
#endif
