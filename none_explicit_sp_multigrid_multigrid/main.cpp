//This follows the tutorial by Oliver Sander (see Introduction to DUNE)
#include "config.h"
#include <dune/common/parallel/mpihelper.hh>

#define HAVE_UG 1
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/matrix.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/yaspgrid.hh>
#include "space_time_mgrid_driver.hh"
#include "helpers.hh"
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include "prolongation.hh"
#include <dune/istl/umfpack.hh>
#include <dune/istl/preconditioners.hh>
#include "smoothers.hh"
#include "loaders.hh"
#include <dune/common/parametertreeparser.hh>
typedef Dune::BlockVector<Dune::FieldVector<double, 1>> VectorType;
typedef Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>> MatrixType;

template <typename NM, typename NVT>
void directSolve(NM &A_j, NVT &xk_j, NVT b_j)
{
    typedef Dune::MatrixAdapter<NM, NVT, NVT> LinearOperator;
    Dune::InverseOperatorResult result;
    Dune::UMFPack<NM> umf(A_j, 2);
    umf.apply(xk_j, b_j, result); //TODO(henrik):Warning
}

template <typename NM, typename NVT>
void CGSolve(NM &A_j, NVT &xk_j, NVT b_j)
{
    typedef Dune::MatrixAdapter<NM, NVT, NVT> LinearOperator;
    Dune::InverseOperatorResult result;
    LinearOperator op(A_j);
    typedef Dune::SeqILU<NM, NVT, NVT> Preconditioner;
    Preconditioner prec(A_j, 1.8);

    const double reduction = 1e-9;
    const int max_iterations = 5000;
    const int verbose = 1;

    typedef Dune::CGSolver<NVT> Solver;
    Solver solver(op, prec, reduction, max_iterations, verbose);
    solver.apply(xk_j, b_j, result);
}
template <class Basis>
void getOccupationPattern(const Basis &basis, Dune::MatrixIndexSet &nb,Dune::MatrixIndexSet &small,Dune::MatrixIndexSet &small2, size_t nOfTimesteps, const bool onlyL)
{
    nb.resize(basis.size() * nOfTimesteps, basis.size() * nOfTimesteps);
    small.resize(basis.size() , basis.size() );
     small2.resize(basis.size() , basis.size() );
    auto gridView = basis.gridView();
    auto localView = basis.localView();
    //auto localIndexSet = basis.makeIndexSet();
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        for (size_t i = 0; i < localView.size(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < localView.size(); j++)
            {
                auto col = localView.index(j);
                small.add(  row, col);
                for (size_t k = 0; k < nOfTimesteps; k++)
                {
                    if(!onlyL) nb.add(k * basis.size() + row, k * basis.size() + col);
                    //add b if possible
                    if (k > 0)
                    {
                        nb.add((k)*basis.size() + row, (k - 1) * basis.size() + col);
                    }
                }
            }
        }
    }
    for (int i = 0; i < basis.size(); i++)
    {
        small2.add(i, i); //TODO(henrik): Reset that and multiply result in getStartHeat
    }
}
template <class LocalView, class MType>
void assembleElementMatrix(const LocalView &localView, MType &elementMatrix, MType &elementMassMatrix)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    auto geometry = element.geometry();
    // Get set of shape functions for this element
    const auto &localFiniteElement = localView.tree().finiteElement();
    elementMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    elementMassMatrix.setSize(localFiniteElement.size(), localFiniteElement.size());
    //init
    elementMatrix = 0;
    elementMassMatrix = 0;
    //calc order
    int order = 2 * (dim * localFiniteElement.localBasis().order() - 1);
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const auto quadPos = quadPoint.position();
        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = geometry.jacobianInverseTransposed(quadPos);
        const auto jacobianNon = geometry.jacobianTransposed(quadPos);
        // The multiplicative factor in the integral transformation formula
        const auto integrationElement = geometry.integrationElement(quadPos);
        // The gradients of the shape functions on the reference element
        std ::vector<Dune::FieldMatrix<double, 1, dim>> referenceGradients;
        std ::vector<Dune::FieldVector<double, 1>> values;
        localFiniteElement.localBasis().evaluateJacobian(quadPos, referenceGradients);
        localFiniteElement.localBasis().evaluateFunction(quadPos, values);
        // Compute the shape function gradients on the real element
        std ::vector<Dune::FieldVector<double, dim>> gradients(referenceGradients.size());
        for (size_t i = 0; i < gradients.size(); i++)
            jacobian.mv(referenceGradients[i][0], gradients[i]);
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                //normally we would now add code for kroneckerproduct.

                //Added second part for matrix
                elementMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += ((gradients[i] * gradients[j]) * quadPoint.weight() * integrationElement) + values[i] * values[j] * quadPoint.weight() * integrationElement;
                //Added second part for matrix
                elementMassMatrix[localView.tree().localIndex(i)][localView.tree().localIndex(j)] += values[i] * values[j] * quadPoint.weight() * integrationElement;
            }
        }
    }
}
// Compute the heatsource term for a single element
template <class LocalView>
void getStartHeat(const LocalView &localView,
                  Dune::BlockVector<Dune::FieldVector<double, 1>> &localRhs,
                  const std::function<double(Dune::FieldVector<double, LocalView::Element::dimension>)> heatTerm)
{
    using Element = typename LocalView::Element;
    auto element = localView.element();
    const int dim = Element::dimension;
    // Set of shape functions for a single element
    const auto &localFiniteElement = localView.tree().finiteElement();
    // Set all entries to zero
    localRhs.resize(localFiniteElement.size());
    localRhs = 0;
    // A quadrature rule
    int order = dim;
    const auto &quadRule = Dune::QuadratureRules<double, dim>::rule(element.type(), order);
    // Loop over all quadrature points
    for (const auto &quadPoint : quadRule)
    {
        // Position of the current quadrature point in the reference element
        const Dune::FieldVector<double, dim> &quadPos = quadPoint.position();
        // The multiplicativefactor in the integraltransformation formula
        const double integrationElement = element.geometry().integrationElement(quadPos);
        double functionValue = heatTerm(element.geometry().global(quadPos));
        // Evaluate all shape function values at this point
        std ::vector<Dune::FieldVector<double, 1>> shapeFunctionValues;
        localFiniteElement.localBasis().evaluateFunction(quadPos, shapeFunctionValues);
        // Actually compute the vector entries
        for (size_t i = 0; i < localRhs.size(); i++)
            localRhs[i] += shapeFunctionValues[i] * functionValue * quadPoint.weight();
    }
}
template <class Basis>
void assembleHeatEquationProblemForMethodOfLinesWithoutInvert(const Basis &basis,
                                                              MatrixType &matrix,
                                                              MatrixType &small,
                                                              MatrixType &mmgrid,
                                                              VectorType &rhs,
                                                              const std::function<double(Dune::FieldVector<double, Basis::GridView::dimension>)> heatTerm,
                                                              const size_t noOfTimesteps,
                                                              const double timestepSize,
                                                              const double h,
                                                              const bool onlyL=false)
{
    auto gridView = basis.gridView();
    //get Occupation pattern
    Dune::MatrixIndexSet occupationPattern,smallOccupationPattern,smallOccupationPattern2;
    getOccupationPattern(basis, occupationPattern,smallOccupationPattern,smallOccupationPattern2, noOfTimesteps,onlyL);
    //and apply it so we can access the entries
    occupationPattern.exportIdx(matrix);
    smallOccupationPattern.exportIdx(small);
    smallOccupationPattern2.exportIdx(mmgrid);
    //occupationPattern.exportIdx(massMatrix);
    matrix = 0;
    small=0;
    mmgrid=0;
    //massMatrix = 0;
    // set rhs to correct length
    rhs.resize(basis.size() * noOfTimesteps);
    //set all rhs to zero
    rhs = 0;
    auto localView = basis.localView();
    // Mark all dirichletNodes
    std ::vector<char> dirichletNodes(basis.size(), false);
    Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
        dirichletNodes[index] = true;
    });
     const double stiffnessTime = 1.0; //donot know the basis :(
                        const double massTime = (timestepSize)*stiffnessTime;
                        //N_tau is constant, because it represents function before/ we use p_t =0
                            const double N_tau = stiffnessTime;
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        Dune::Matrix<Dune::FieldMatrix<double, 1, 1>> elementMatrix, elementMassMatrix;
        assembleElementMatrix(localView, elementMatrix, elementMassMatrix);
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                auto col = localView.index(j);
                //if ((dirichletNodes[row]==false && dirichletNodes[col]==false))
                {
                    //massTime and stiffnessTime are constant, because we are only implementing backward euler
                        small[ row][ col] += massTime * elementMatrix[i][j] + stiffnessTime * elementMassMatrix[i][j];
                    for (size_t k = 0; k < noOfTimesteps; k++)
                    {
                        if(!onlyL) matrix[k * basis.size() + row][k * basis.size() + col] += massTime * elementMatrix[i][j] + stiffnessTime * elementMassMatrix[i][j];
                        //adding B_tau_h if possible
                        if (k > 0)
                        {
                            matrix[(k)*basis.size() + row][(k - 1) * basis.size() + col] += -N_tau * elementMassMatrix[i][j];
                        }
                        //massMatrix[row][col] += elementMassMatrix[i][j];
                    }
                }
            }
        }
        for(int i =0;i< mmgrid.N();i++){
            mmgrid[i][i]=   massTime*2.0/h + stiffnessTime *h*2.0/3.0;
        }
        // Now get the localcontribution to the right −hand side vector
        Dune::BlockVector<Dune::FieldVector<double, 1>> localRhs;
        getStartHeat(localView, localRhs, heatTerm);
        for (size_t i = 0; i < localRhs.size(); i++)
        {
            // The global index of the i −th vertex
            auto row = localView.index(i);
            if (dirichletNodes[row] == false)
                rhs[row] += localRhs[i];
        }
    }
    // Mark all dirichletNodes
    //boundary cdt taken from https://github.com/dune-project/dune-functions/blob/master/examples/poisson-pq2.cc
    Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
        //dirichletNodes[index] = true;
        rhs[index] = 0.0;
        {
                auto cIt = small[ + index].begin();
                    auto cEndIt = small[index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()))?1.0:0.0;
                    }
                }
                {
                auto cIt = mmgrid[  index].begin();
                    auto cEndIt = mmgrid[index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()))?1.0:0.0;
                    }
                }
        for (size_t k = 0; k < noOfTimesteps; k++)
        {
            {
                auto cIt = matrix[k * basis.size() + index].begin();
                    auto cEndIt = matrix[k * basis.size() + index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()-k*basis.size()))?1.0:0.0;
                    }
                }
                if(k>0){
                    auto cIt = matrix[(k-1) * basis.size() + index].begin();
                    auto cEndIt = matrix[(k-1) * basis.size() + index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()-(k-1)*basis.size()))?1.0:0.0;
                    }
                }
        }
    });
}

template <class Basis>
void assembleHeatEquationProblemForMethodOfLinesWithoutInvert2(const Basis &basis,
                                                              MatrixType &matrix,
                                                              MatrixType &small,
                                                              MatrixType &mmgrid,
                                                              VectorType &rhs,
                                                              const std::function<double(Dune::FieldVector<double, Basis::GridView::dimension>)> heatTerm,
                                                              const size_t noOfTimesteps,
                                                              const double timestepSize,
                                                              const double h,
                                                              const bool onlyL=false)
{
    auto gridView = basis.gridView();
    //get Occupation pattern
    Dune::MatrixIndexSet occupationPattern,smallOccupationPattern,smallOccupationPattern2;
    getOccupationPattern(basis, occupationPattern,smallOccupationPattern,smallOccupationPattern2, noOfTimesteps,onlyL);
    //and apply it so we can access the entries
    occupationPattern.exportIdx(matrix);
    smallOccupationPattern.exportIdx(small);
    smallOccupationPattern2.exportIdx(mmgrid);
    //occupationPattern.exportIdx(massMatrix);
    matrix = 0;
    small=0;
    mmgrid=0;
    //massMatrix = 0;
    // set rhs to correct length
    rhs.resize(basis.size() * noOfTimesteps);
    //set all rhs to zero
    rhs = 0;
    auto localView = basis.localView();
    // Mark all dirichletNodes
    std ::vector<char> dirichletNodes(basis.size(), false);
    Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
        dirichletNodes[index] = true;
    });
     const double stiffnessTime = 1.0; //donot know the basis :(
                        const double massTime = (timestepSize)*stiffnessTime;
                        //N_tau is constant, because it represents function before/ we use p_t =0
                            const double N_tau = stiffnessTime;
    for (const auto &element : Dune::elements(gridView))
    {
        localView.bind(element);
        Dune::Matrix<Dune::FieldMatrix<double, 1, 1>> elementMatrix, elementMassMatrix;
        assembleElementMatrix(localView, elementMatrix, elementMassMatrix);
        for (size_t i = 0; i < elementMatrix.N(); i++)
        {
            auto row = localView.index(i);
            for (size_t j = 0; j < elementMatrix.M(); j++)
            {
                auto col = localView.index(j);
                //if ((dirichletNodes[row]==false && dirichletNodes[col]==false))
                {
                    //massTime and stiffnessTime are constant, because we are only implementing backward euler
                        if(!onlyL)
                        small[ row][ col] += massTime * elementMatrix[i][j] + stiffnessTime * elementMassMatrix[i][j];
                        else small[row][ col] +=  N_tau*elementMassMatrix[i][j];
                    for (size_t k = 0; k < noOfTimesteps; k++)
                    {
                        if(!onlyL) matrix[k * basis.size() + row][k * basis.size() + col] += massTime * elementMatrix[i][j] + stiffnessTime * elementMassMatrix[i][j];
                        //adding B_tau_h if possible
                        if (k > 0)
                        {
                            matrix[(k)*basis.size() + row][(k - 1) * basis.size() + col] += -N_tau * elementMassMatrix[i][j];
                        }
                        //massMatrix[row][col] += elementMassMatrix[i][j];
                    }
                }
            }
        }
        for(int i =0;i< mmgrid.N();i++){
            mmgrid[i][i]=   massTime*2.0/h + stiffnessTime *h*2.0/3.0;
        }
        // Now get the localcontribution to the right −hand side vector
        Dune::BlockVector<Dune::FieldVector<double, 1>> localRhs;
        getStartHeat(localView, localRhs, heatTerm);
        for (size_t i = 0; i < localRhs.size(); i++)
        {
            // The global index of the i −th vertex
            auto row = localView.index(i);
            if (dirichletNodes[row] == false)
                rhs[row] += localRhs[i];
        }
    }
    // Mark all dirichletNodes
    //boundary cdt taken from https://github.com/dune-project/dune-functions/blob/master/examples/poisson-pq2.cc
    Dune::Functions::forEachBoundaryDOF(basis, [&](auto &&index) {
        //dirichletNodes[index] = true;
        rhs[index] = 0.0;
        {
                auto cIt = small[ + index].begin();
                    auto cEndIt = small[index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()))?1.0:0.0;
                    }
                }
                {
                auto cIt = mmgrid[  index].begin();
                    auto cEndIt = mmgrid[index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()))?1.0:0.0;
                    }
                }
        for (size_t k = 0; k < noOfTimesteps; k++)
        {
            {
                auto cIt = matrix[k * basis.size() + index].begin();
                    auto cEndIt = matrix[k * basis.size() + index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()-k*basis.size()))?1.0:0.0;
                    }
                }
                if(k>0){
                    auto cIt = matrix[(k-1) * basis.size() + index].begin();
                    auto cEndIt = matrix[(k-1) * basis.size() + index].end();
                    // loop over nonzero matrix entries in current row
                    for (; cIt != cEndIt; ++cIt)
                    {
                        *cIt=(index ==(cIt.index()-(k-1)*basis.size()))?1.0:0.0;
                    }
                }
        }
    });
}
template<const int dim, typename GridType> void
test_function(Dune::ParameterTree &pt,bool cube){
        const int SPACE_J=pt.get<int>("refine",3);
        const int TIME_i = pt.get<int>("time_refine",9);
        const int NUMBER_OF_SOLVES = pt.get<int>("k",1);
        const int noOfTimeSteps = pow(2,TIME_i);
        const double mu_crit_0 = sqrt(2)/3;//because ofg backward euler
        typedef Dune::FieldVector<double, dim> V2;
        V2 ll, ur;
        for(int i=0;i<dim;i++)
        ur[i] = 1;
        std::array<unsigned, dim> ar;
        const size_t W = 2;
        for(int i=0;i<dim;i++)
        ar[i] = W;
        //ar[1] = W;
         double T = pt.get<double>("T",1);
         auto grid2 = cube ?Dune::StructuredGridFactory<GridType>::createCubeGrid(ll, ur, ar):Dune::StructuredGridFactory<GridType>::createSimplexGrid(ll, ur, ar);
        auto grid = grid2.get();
        grid->globalRefine(SPACE_J+1);
        typedef typename GridType::LeafGridView GridView;
        typedef typename GridType::LevelGridView GV;

        Dune::Functions::LagrangeBasis<GV, 1> fineBasis(grid->levelGridView(SPACE_J));
        typedef  Dune::Functions::LagrangeBasis<GV, 1> Basis;
        auto sourceTerm = [](const Dune::FieldVector<double, dim> &x) {
            if (std::abs(x[0] - 0.5) < 0.25 && std::abs(x[1] - 0.5) < 0.25)
                return 1.0;
            return 0.0;
        };
        VectorType rhs,rhs1,rhs2;
        MatrixType matrix,small,mm; //, massMatrix;
        assembleHeatEquationProblemForMethodOfLinesWithoutInvert2(fineBasis, matrix,
                                                                 small,mm,
                                                                 rhs, sourceTerm, noOfTimeSteps, T/((double)noOfTimeSteps),pow(.5,(1.0+SPACE_J)),true);//want mass matrix
        rhs1.resize(small.N());
        rhs2.resize(small.N());
        for(int i=0;i<rhs1.size();i++){
            rhs1[i]=rhs[i];
        }
        if(pt.hasKey("i")){
            loadFromFile(rhs1,pt.get<std::string>("i","input.vector"));
        }
        small.mv(rhs1,rhs2);
        rhs=0;
        for(int i=0;i<rhs2.size();i++){
            rhs[i]=rhs2[i];
        }
        //MatrixType restriction;
        //getTimeRestriction(basis,2000,0.001,0.001,restriction);
        VectorType u_0,u_02;
        u_0.resize(rhs.size());
        u_02.resize(rhs.size());
        /*typedef RichardsonIterationSmoother<MatrixType,VectorType> RSmoother;
        RSmoother smoother(250);*/
        typedef Dune::SeqJac<MatrixType,VectorType,VectorType > SeqJac;
        typedef SmootherProxy<SeqJac,MatrixType> RSmoother;
        int smootherCalls =pt.get<int>("nux",2);
        int smootherCallsTime =pt.get<int>("nut",2);
        const int solveTimeAtLevel = pt.get<int>("solveTimeAtLevel",0);
        const size_t cycleGamma = pt.get<size_t>("gamma",1);
        RSmoother smoother([smootherCalls](const MatrixType &m,size_t j){
            return SeqJac(m,smootherCalls,2.0/3.0); //as given in paper, this is the inner smoother for spatial
            //be aware that this is very sensitive to the time scale T
        });
        typedef SpaceTimeMultiGridMethodDriver<Dune::Functions::LagrangeBasis<GV, 1>,GridType,MatrixType,VectorType,RSmoother,RSmoother,double> SpacetimeDriver;
        SpacetimeDriver space_time([&sourceTerm,T](int time_i,int space_j, Basis &b){
            MatrixType matrix,small,mm; //, massMatrix;
            VectorType rhs;
        assembleHeatEquationProblemForMethodOfLinesWithoutInvert(b, matrix,
                                                                 small,mm,
                                                                 rhs, sourceTerm, pow(2,time_i), T*pow(0.5,(double) time_i),pow(.5,(1.0+space_j)));

        return matrix;
        },
        [&sourceTerm,T](int time_i,int space_j, Basis &b){
            MatrixType matrix,small,mm; //, massMatrix;
            VectorType rhs;
        assembleHeatEquationProblemForMethodOfLinesWithoutInvert(b, matrix,
                                                                 small,mm,
                                                                 rhs, sourceTerm, pow(2,time_i), T*pow(0.5,(double) time_i),pow(.5,(1.0+space_j)),true);

        return matrix;
        },
        [&sourceTerm,T](int time_i,int space_j,Basis &b){
            MatrixType matrix,small,mmgrid; //, massMatrix;
            VectorType rhs;
        assembleHeatEquationProblemForMethodOfLinesWithoutInvert(b, matrix,
                                                                 small,
                                                                 mmgrid,
                                                                 rhs, sourceTerm, pow(2,time_i), T*pow(0.5,(double) time_i),pow(.5,(1.0+space_j)));

        return mmgrid;
        },
        [&sourceTerm,T](int time_i,int space_j,Basis &b){
            MatrixType matrix,small,mmgrid; //, massMatrix;
            VectorType rhs;
        assembleHeatEquationProblemForMethodOfLinesWithoutInvert(b, matrix,
                                                                 small,
                                                                 mmgrid,
                                                                 rhs, sourceTerm, pow(2,time_i), T*pow(0.5,(double) time_i),pow(.5,(1.0+space_j)));

        return small;
        },
        grid,SPACE_J,smoother,smoother,mu_crit_0,T,[](size_t j){
            return pow(.5,(j));//WARN(henrik): should be rewritten, only works on const //TODO(henrik) RETHINK
        },[T](size_t j){
            return T*pow(0.5,j);
        },smootherCallsTime,solveTimeAtLevel,cycleGamma);
        double prevE= 0.0;
        double maxV=0;
        u_02.resize(u_0.size());
        for(int i=0;i<NUMBER_OF_SOLVES;i++){
            std::stringstream s;
            s<<i+1;
        space_time.solve(u_0,rhs,TIME_i,SPACE_J);
        writeToFile(u_0,pt.get<std::string>("o","output.test")+".v"+s.str());
        if(pt.hasKey("conv")){
        matrix.mv(u_0,u_02);
        double err = u_02.two_norm();

        std::cout<<"prevE:"<<prevE<<std::endl;
        std::cout<<"err:"<<err<<std::endl;
        std::cout<<"diff:"<<abs(err-prevE)<<" ,quot:"<<err/prevE<<std::endl;
        if((i!=0 && (err/prevE)>maxV)||i==1){
            maxV=err/prevE;
        }
        if(abs(err-prevE)/err<1e-12){
            std::cout<<"k"<<i<<std::endl;
            std::cout<<maxV<<std::endl;
            return ;
        }
        std::cout<<"M:"<<maxV<<std::endl;
        std::cout<<abs(err-prevE)/prevE<<std::endl;
        prevE= err;
        }
        }
        VectorType resultStep2,resultStep;
        resultStep2.resize(fineBasis.size());
        resultStep.resize(fineBasis.size());
        for (size_t i = 0; i < noOfTimeSteps; i++)
        {
            for (size_t k = 0; k < fineBasis.size(); k++)
            {
                resultStep2[k] = u_0[i * fineBasis.size() + k];
                //resultStep[k] = u_02[i * fineBasis.size() + k];

            }
            Dune::VTKWriter<GV> vtkWriter(fineBasis.gridView());
            vtkWriter.addVertexData(resultStep2, "solution");
            //vtkWriter.addVertexData(resultStep, "exact");
            std::stringstream str;
            str << i;
            vtkWriter.write("jac50_10_multigrid" + str.str());
        }
        auto start = std::chrono::high_resolution_clock::now();
        space_time.solve(u_02,rhs,TIME_i,SPACE_J);
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> dur = end-start;
        std::cout<<dur.count()<<std::endl;
        writeToFile(u_0,pt.get<std::string>("o","output.test"));
}
int main(int argc, char **argv)
{
    Dune::MPIHelper::instance(argc, argv);
    //try
    {
        Dune::ParameterTree pt;
        Dune::ParameterTreeParser::readOptions(argc,argv,pt);
       if(pt.hasKey("threed")){
           const int dim = 3;
            if(pt.hasKey("cube")){
                typedef Dune::YaspGrid<dim> GridType;
                test_function<dim,GridType>(pt,true);
        } else {
            typedef Dune::UGGrid<dim> GridType;
                test_function<dim,GridType>(pt,false);
        }
       }
       if(pt.hasKey("twod")){
           const int dim = 2;
            if(pt.hasKey("cube")){
                typedef Dune::YaspGrid<dim> GridType;
                test_function<dim,GridType>(pt,true);
        } else {
            typedef Dune::UGGrid<dim> GridType;
                test_function<dim,GridType>(pt,false);
        }
       }
        if(pt.get<int>("oned",0)==1){
           const int dim = 1;
            if(pt.hasKey("cube")){
                typedef Dune::YaspGrid<dim> GridType;
                test_function<dim,GridType>(pt,true);
        } else {
            std::cerr<<"Simplices not supported in one D."<<std::endl;
        }
       }
    }
}