#include <dune/pdelab/constraints/conforming.hh>
class BCTypeParam
    : public Dune::PDELab::DirichletConstraintsParameters
{
    double time;

public:
    //! boundary condition type function (true = Dirichlet)
  template<typename I, typename X>
  bool isDirichlet(const I& i, const X& x) const
  {
    return true;
  }

    //! set time for subsequent evaluation
    void setTime(double t)
    {
        time = t;
    }
};