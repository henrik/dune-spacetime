#include <dune/pdelab.hh>
#include "prolongation.hh"
typedef Dune::BlockVector<Dune::FieldVector<double, 1>> VectorType;
typedef Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>> MatrixType;

/** Manage a hierarchy of assemblers
 */
template<typename FS,typename LOP,Dune::SolverCategory::Category st = Dune::SolverCategory::sequential>
class HeatEquationProblemAssemblerHierarchy
{
public:
  // export types
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  /*typedef Dune::PDELab::GridOperator<typename FS::GFS,typename FS::GFS,LOP,MBE,
                                     typename FS::NT,typename FS::NT,typename FS::NT,
                                     typename FS::CC,typename FS::CC> GO;
  */
    typedef Dune::PDELab::GridOperator<typename FS::GFS,typename FS::GFS,LOP,MBE,
                                     typename FS::NT,typename FS::NT,typename FS::NT,
                                     typename FS::CC,typename FS::CC> GO;
  typedef typename GO::Jacobian MAT;
  typedef VectorHierarchy<FS> VH;
  typedef MatrixHierarchy<HeatEquationProblemAssemblerHierarchy<FS,LOP,st> > MH;

  HeatEquationProblemAssemblerHierarchy (const FS& fs,LOP& lop,const std::size_t nonzeros) : gop(fs.maxLevel()+1)
  {
    for (int j=0; j<=fs.maxLevel(); j++)//TODO: update generation
      gop[j] = std::make_shared<GO>(fs.getGFS(0,j),fs.getCC(0,j),fs.getGFS(0,j),fs.getCC(0,j),lop,MBE(nonzeros));
  }

  int maxLevel () const
  {
    return gop.size()-1;
  }

  // return grid reference
  GO& getGO (int j)
  {
    if (j>=0 && j<gop.size())
      return *gop[j];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // return grid reference const version
  const GO& getGO (int j) const
  {
    if (j>=0 && j<gop.size())
      return *gop[j];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // evaluate jacobian on all levels
  void jacobian (const VH& xh, MH& mh)
  {
    for (int j=0; j<=maxLevel(); j++)
      {
        mh[j] = 0.0;
        //gop[j]->jacobian(xh[j],mh[j]);
      }
  }

private:
  std::vector<std::shared_ptr<GO> > gop;
};