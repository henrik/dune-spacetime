#ifndef SPACE_TIME_MGRID_DRIVER_HH
#define SPACE_TIME_MGRID_DRIVER_HH
#include "thread_job_organizer.hh"
//TODO: 1. Time coarsening --> Reorganize Grid and matrix hierarchy
// 2. Full Coarse --> dito
// Solve with p_t=0 first, later dynamically
// Uses ThreadJobOrganizer to run in parallel index is two dimensional to account for time in first and space in second parameter
//this is based on parsolve MultigridOnOverlappinggrids
template <class GridHierarchy, class MatrixHierarchy, class VectorType, class PreSmoother, class PostSmoother, typename ctype>
class SpaceTimeMultiGridMethodDriver: ThreadJobOrganizer<size_t,VectorType>
{
    typedef typename MatrixHierarchy::Matrix M;
    typedef Dune::PDELab::Backend::Native<VectorType> NVT;
    typedef Dune::PDELab::Backend::Native<M> NM;
    SpaceTimeProlongationOperatorHierarchy<GridHierarchy> poh;
    //    size_t nu_1, nu_2;
    const MatrixHierarchy &A;
    const GridHierarchy &gh;
    PreSmoother &preSmoother; //not const, because they might be adaptive or so
    PostSmoother &postSmoother;
    int J;
    ctype mu_critical;
    std::function<ctype(size_t)>  tau_L;
    std::function<ctype(size_t)> h_L;
public:
    SpaceTimeMultiGridMethodDriver(const GridHierarchy &_gh, const MatrixHierarchy &_A, PreSmoother &_pre_smoother, PostSmoother &_post_smoother, ctype _mu_critical,std::function<ctype(size_t)> _h_L,std::function<ctype(size_t)> _tau_L) : gh(_gh),
                                                                                                                                     preSmoother(_pre_smoother), postSmoother(_post_smoother), A(_A), poh(_gh), J(gh.maxLevel()),mu_critical(_mu_critical), tau_L(_tau_L),h_L(_h_L)
    {
    }
    //d is the start value
    //xk_j the initial guess
    //tau_L is the time steps Length
    // h_L is the discretization length, deps on grid, so it is a function
    //time_i is the time resolution in log scale
    //T is the big time
    void solve(VectorType xk_j,  VectorType b_j,size_t time_i, size_t space_j, ctype T)
    {
        using Dune::PDELab::Backend::native;
        if(time_i<=0){
            DUNE_THROW(Dune::Exception, "time level out of range");
        }
        if (space_j == 0)
        {
            std::cout << "SOLVING" << std::endl;

            // Solve linear system taken from sheet 02 FEM 201920
            //later replaced by simple UMFPACK
           /* typedef Dune::MatrixAdapter<NM, NVT, NVT> LinearOperator;
            LinearOperator op(native(A_j));
            typedef Dune::SeqILU<NM, NVT, NVT> Preconditioner;
            Preconditioner prec(native(A_j), 1.8);

            const ctype reduction = 1e-9;
            const int max_iterations = 10000;
            const int verbose = 1;

            typedef Dune::CGSolver<NVT> Solver;
            Solver solver(op, prec, reduction, max_iterations, verbose);
            */

           auto A_j = A.at(time_i,space_j);//get matrix from the appropiate time stepping/space
           //Forward substitution or direct solve whole system?
           Dune::UMFPack<NM> solver(native(A_j),0);
            Dune::InverseOperatorResult result;
            solver.apply(native(xk_j), native(b_j), result);
            this->save(time_i,space_j,new VectorType(xk_j));
            return;
        }

        ctype mu_L= tau_L(time_i)*pow(h_L(space_j),-2);//TODO: params
        //entirely sequential
        //TODO(henrik): what happens if you
        if(mu_L<mu_critical) {
            //semi coarsening in time
            this->semiCoarsening(xk_j, b_j,time_i, space_j,T);
        } else {
            //full space time coarsening
            this->fullCoarsening(xk_j, b_j,time_i,space_j,T);
        }
        this->save(time_i,space_j,new VectorType(xk_j));//BUG(henrik): Memory management!
    }

private:
    void fullCoarsening(VectorType &xk_j,  VectorType b_j,size_t time_i, size_t space_j,ctype T){

        using Dune::PDELab::Backend::native;
        auto A_j = A.at(time_i,space_j);//get matrix from the appropiate time stepping/space
        //auto xk_j1 = xk_j;
        preSmoother.apply(xk_j, b_j, A_j, time_i,space_j);
        VectorType d_j(gh.getGFS(time_i,space_j));
        d_j = b_j;
        native(A_j).mmv(native(xk_j), native(d_j)); //defect calculation
        //CGM R;//coarsening matrix
        VectorType d_j_1(gh.getGFS(time_i-1,space_j-1), 0.0);//space is kept constant, time has no impact for pt=0 ==> backward euler
        poh.at(time_i,space_j,true).mtv(native(d_j), native(d_j_1)); // restriction
        //TODO(henrik): currently V-Cycle only here
        VectorType x_j_1(gh.getGFS(time_i-1,space_j), 0.0);//space is kept constant, time has no impact for pt=0==>backward euler
        //full time space coarsening
        solve(x_j_1,d_j_1,time_i-1,space_j-1,T);//TODO(henrik): add to timestep extratime
        VectorType y_j(gh.getGFS(time_i,space_j), 0.0);
        poh.at(time_i,space_j,true).mv(native(x_j_1), native(y_j));
        xk_j += y_j;                           //coarse grid correction;
                                               //Dune::printvector(datei, xk_j1, "xk_j1", "", 1, 24, 4);
                                               //TODO(henrik): avoid copys
        postSmoother.apply(xk_j, b_j, A_j, time_i,space_j);
    }
    //Coarsens in time
    void semiCoarsening(VectorType &xk_j,  VectorType b_j, size_t time_i,size_t space_j, ctype T){
        using Dune::PDELab::Backend::native;
        auto A_j = A.at(time_i,space_j);//get matrix from the appropiate time stepping/space
        //auto xk_j1 = xk_j;
        preSmoother.apply(xk_j, b_j, A_j, time_i,space_j);
        VectorType d_j(gh.getGFS(time_i,space_j));
        d_j = b_j;
        native(A_j).mmv(native(xk_j), native(d_j)); //defect calculation
        //CGM R;//coarsening matrix
        VectorType d_j_1(gh.getGFS(time_i-1,space_j), 0.0);//space is kept constant, time has no impact for pt=0 ==> backward euler
        //return time coarsening
        //Nt=1 => M_{tau_L}\tilde{M}^{1,2}_{tau_L}=[1]//because pt =0, so they are constant
        //=>
        poh.at(time_i,space_j,false).mtv(native(d_j), native(d_j_1)); // restriction
        //TODO(henrik): currently V-Cycle only here
        VectorType x_j_1(gh.getGFS(time_i-1,space_j), 0.0);//space is kept constant, time has no impact for pt=0==>backward euler
        //only time coarsening
        solve(x_j_1,d_j_1,time_i-1,space_j,T);//TODO(henrik): add to timestep extratime
        VectorType y_j(gh.getGFS(time_i,space_j), 0.0);
        poh.at(time_i,space_j,false).mv(native(x_j_1), native(y_j));
        xk_j += y_j;                           //coarse grid correction;
                                               //Dune::printvector(datei, xk_j1, "xk_j1", "", 1, 24, 4);
                                               //TODO(henrik): avoid copys
        postSmoother.apply(xk_j, b_j, A_j, time_i,space_j);

    }
};
#endif
