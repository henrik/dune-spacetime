#ifndef THREAD_JOB
#define THREAD_JOB
#include <map>
#include <vector>
#include <functional>

template<typename Index, class ResultType>
class ThreadJobOrganizer{
  private:
    std::map<Index,std::map<Index,ResultType*>> results;
    std::vector<std::pair<Index,std::function<ResultType(Index)> > > tasks;
  public:
    ThreadJobOrganizer(){}
    ResultType result(Index i,Index j){
      return *results[i][j];
    }
    //Submit some work to the organizer, no dependency tracking yet
    void submit(std::function<ResultType(Index)> work, Index id){
        tasks.push_back(std::pair<Index,std::function<ResultType(Index)> >(id,work));
    }
    void save(Index id,Index id2, ResultType* res) {
      if(results.count(id)==0){
        results[id]=std::map<Index,ResultType*>();
      }
      results[id][id2]=res;
    }
    void work() {
        //TODO:parallize functions
        while(!tasks.empty()) {
            auto task = tasks.pop_back();
            results[task.first] = task.second();
        }
    }
};
#endif