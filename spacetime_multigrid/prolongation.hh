
#ifndef PROLONGATION_HH
#define PROLONGATION_HH
#include <dune/pdelab/boilerplate/pdelab.hh>

#include <utility>
#include <vector>
#include <set>
#include <map>

#include <dune/pdelab/boilerplate/pdelab.hh>

//taken from geometric_multigrid_components.hh from dune-parsolve, because Library didn't compile
//TODO(henrik): fix that
/** Hierarchy of continuous Lagrange finite element spaces

    Constructs a grid function space for all levels based on level grid views.
    Setting the current level allows the use like a rid function
    space constructed on the leaf view without giveing further parameters.
*/
template <typename T, typename N, unsigned int degree, typename BCType,
          Dune::GeometryType::BasicType gt,
          Dune::PDELab::MeshType mt, Dune::SolverCategory::Category st = Dune::SolverCategory::sequential,
          typename VBET = Dune::PDELab::ISTL::VectorBackend<>>
class CGSpaceHierarchy
{
public:
  // export types
  typedef T Grid;
  typedef typename T::LevelGridView GV;
  typedef typename T::ctype ctype;
  static constexpr int dim = T::dimension;
  static constexpr int dimworld = T::dimensionworld;

  typedef Dune::PDELab::CGFEMBase<GV, ctype, N, degree, dim, gt> FEMB;
  typedef Dune::PDELab::CGCONBase<Grid, degree, gt, mt, st, BCType> CONB;

  typedef typename FEMB::FEM FEM;
  typedef typename CONB::CON CON;

  typedef VBET VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON, VBE> GFS;

  typedef N NT;
  //BUG(henrik): use dune pdelab
  using DOF = Dune::BlockVector<Dune::BlockVector<N> >;
  typedef Dune::PDELab::DiscreteGridFunction<GFS, DOF> DGF;
  typedef typename GFS::template ConstraintsContainer<N>::Type CC;
  typedef Dune::PDELab::VTKGridFunctionAdapter<DGF> VTKF;

  // constructor making the grid function space an all that is needed
  //tau_0 is the smallest timesteps, we operate on a 2^n time frame+ we don't know how far we are going
  // so we just allow anything, and calc later, this is because of p_t=0 (implicit euler)
  CGSpaceHierarchy(Grid &grid_, const BCType &bctype, size_t time_levels)
      : grid(grid_), gvp(time_levels), fembp(time_levels), conbp(time_levels),
        gfsp(time_levels), ccp(time_levels)
  {
    for (int i = 0; i < time_levels; i++)
    {
      gvp[i] = std::vector<std::shared_ptr<GV>>(grid_.maxLevel()+1);
      fembp[i] = std::vector<std::shared_ptr<FEMB>>(grid_.maxLevel()+1);
      conbp[i] = std::vector<std::shared_ptr<CONB>>(grid_.maxLevel()+1);
      gfsp[i] = std::vector<std::shared_ptr<GFS>>(grid_.maxLevel()+1);
      ccp[i] = std::vector<std::shared_ptr<CC>>(grid_.maxLevel()+1);
      for (int j = 0; j <= grid.maxLevel(); j++)
      {
        gvp[i][j] = std::make_shared<GV>(grid.levelGridView(j));
        fembp[i][j] = std::make_shared<FEMB>(*gvp[i][j]);
        conbp[i][j] = std::make_shared<CONB>(grid, bctype);
        gfsp[i][j] = std::make_shared<GFS>(*gvp[i][j], fembp[i][j]->getFEM(), conbp[i][j]->getCON());
        conbp[i][j]->postGFSHook(*gfsp[i][j]);
        ccp[i][j] = std::make_shared<CC>();
      }
    }
  }

  int maxLevel() const
  {
    return grid.maxLevel();
  }

  GV &getGV(int i,int j)
  {
    return *gvp[i][j];
  }

  const GV &getGV(int i, int j) const
  {
    return *gvp[i][j];
  }

  FEM &getFEM(int i, int j)
  {
    return fembp[i][j]->getFEM();
  }

  const FEM &getFEM(int i,int j) const
  {
    return fembp[i][j]->getFEM();
  }

  // return gfs reference
  // i is the time frame, currently no difference because of implicit euler
  GFS &getGFS(int i, int j)
  {
    return *gfsp[i][j];
  }

  // return gfs reference const version
  const GFS &getGFS(int i, int j) const
  {
    return *gfsp[i][j];
  }

  // return gfs reference
  CC &getCC(int i,int j)
  {
    return *ccp[i][j];
  }

  // return gfs reference const version
  const CC &getCC(int i,int j) const
  {
    return *ccp[i][j];
  }

  void assembleConstraints(int j, const BCType &bctype)
  {
    ccp[j]->clear();
    Dune::PDELab::constraints(bctype, *gfsp[j], *ccp[j]);
  }

  void assembleAllConstraints(const BCType &bctype)
  {
    for(int i=0; i< ccp.size();i++) {
    for (int j = 0; j <= grid.maxLevel(); j++)
    {
      ccp[i][j]->clear();
      Dune::PDELab::constraints(bctype, *gfsp[i][j], *ccp[i][j]);
    }
    }
  }

  void clearConstraints(int j)
  {
    ccp[j]->clear();
  }

  void clearAllConstraints()
  {
    for (int j = 0; j <= grid.maxLevel(); j++)
      ccp[j]->clear();
  }

  void setConstrainedDOFS(int j, DOF &x, NT nt) const
  {
    Dune::PDELab::set_constrained_dofs(*ccp[j], nt, x);
    conbp[j]->make_consistent(*gfsp[j], x);
  }

  void setNonConstrainedDOFS(int j, DOF &x, NT nt) const
  {
    Dune::PDELab::set_nonconstrained_dofs(*ccp[j], nt, x);
    conbp[j]->make_consistent(*gfsp[j], x);
  }

  void copyConstrainedDOFS(int j, const DOF &xin, DOF &xout) const
  {
    Dune::PDELab::copy_constrained_dofs(*ccp[j], xin, xout);
    conbp[j]->make_consistent(*gfsp[j], xout);
  }

  void copyNonConstrainedDOFS(int j, const DOF &xin, DOF &xout) const
  {
    Dune::PDELab::copy_nonconstrained_dofs(*ccp[j], xin, xout);
    conbp[j]->make_consistent(*gfsp[j], xout);
  }

private:
  Grid &grid;
  std::vector<std::vector<std::shared_ptr<GV>>> gvp;
  std::vector<std::vector<std::shared_ptr<FEMB>>> fembp;
  std::vector<std::vector<std::shared_ptr<CONB>>> conbp;
  std::vector<std::vector<std::shared_ptr<GFS>>> gfsp;
  std::vector<std::vector<std::shared_ptr<CC>>> ccp;
  CGSpaceHierarchy(const CGSpaceHierarchy &other) {} // make copy constructor private
};

/** multigrid transfer operator assembled as a matrix

    Uses a grid function space hierarchy to construct prolongation matrix
    between two CONSECUTIVE levels of the hierarchy. Works for arbitrary
    polynomial degree and spatial dimension.
 */
template <typename GFSH>
class SpaceTimeProlongationOperator : public Dune::BCRSMatrix<Dune::FieldMatrix<typename GFSH::NT, 1, 1>>
{
  typedef typename GFSH::NT E;
  typedef Dune::FieldMatrix<E, 1, 1> M;
  typedef typename GFSH::GFS GFS;
  typedef typename GFS::Traits::GridViewType GV;
  typedef typename GV::Grid::ctype ctype;
  typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;
  typedef typename GV::Traits::template Codim<0>::Entity Element;
  typedef typename Dune::BCRSMatrix<M>::size_type size_type;
  typedef std::set<size_type> IndexSet;
  typedef std::vector<IndexSet> Graph;

  class CoordinateEvaluation
  {
  public:
    struct Traits
    {
      typedef double RangeType;
    };
    // store the coordinate to evaluate
    CoordinateEvaluation(int i_) : i(i_) {}

    // eval coordinate i
    template <typename DT, typename RT>
    inline void evaluate(const DT &x, RT &y) const
    {
      y = x[i];
      return;
    }

  private:
    int i;
  };

public:
  typedef E ElementType;
  typedef Dune::BCRSMatrix<M> BaseT;

  SpaceTimeProlongationOperator(const GFSH &gfsh_, int level_)
      : BaseT(gfsh_.getGFS(0, level_).globalSize(), gfsh_.getGFS(0, level_ - 1).globalSize(),
              Dune::BCRSMatrix<M>::random),
        gfsh(gfsh_), level(level_)
  {
    // check level
    if (level <= 0 || level > gfsh.maxLevel())
      DUNE_THROW(Dune::Exception, "level out of range");

    constexpr int dim = GV::dimension;

    // gfs and matrix graph
    // std::shared_ptr<const GFS> pgfsf(gfsh_.getGFS(level));
    // std::shared_ptr<const GFS> pgfsc(gfsh_.getGFS(level-1));
    const GFS &gfsf = gfsh_.getGFS(0, level);     // fine gfs
    const GFS &gfsc = gfsh_.getGFS(0, level - 1); // coarse gfs
    Graph graph(gfsf.globalSize());               // matrix graph

    // make a vector on the fine grid containing the global indices
    using X = Dune::PDELab::Backend::Vector<GFS, E>;
    using Dune::PDELab::Backend::native;
    X xf(gfsf, 0.0);
    for (typename X::size_type i = 0; i < xf.N(); i++)
      native(xf)[i] = i;
    X xc(gfsc, 0.0);
    for (typename X::size_type i = 0; i < xc.N(); i++)
      native(xc)[i] = i;

    // make local function spaces
    typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
    LFS lfsf(gfsf);
    LFS lfsc(gfsc);
    typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
    LFSCache lfsf_cache(lfsf);
    LFSCache lfsc_cache(lfsc);
    typedef typename X::template LocalView<LFSCache> LView;
    LView lviewf(xf);
    LView lviewc(xc);

    // loop over fine grid to get matrix graph
    for (const auto &cell : elements(gfsf.gridView()))
    {
      // bind local function spaces to element in fine and coarse grid
      lfsf.bind(cell);
      lfsf_cache.update();
      auto father = cell.father();
      lfsc.bind(father);
      lfsc_cache.update();

      // get global indices from the helper vector
      std::vector<E> indexf(lfsf.size());
      lviewf.bind(lfsf_cache);
      lviewf.read(indexf);
      lviewf.unbind();
      std::vector<E> indexc(lfsc.size());
      lviewc.bind(lfsc_cache);
      lviewc.read(indexc);
      lviewc.unbind();

      // determine local coordinates of vertices in fine element
      std::vector<Dune::FieldVector<ctype, dim>> local_position(lfsf.size());
      for (int k = 0; k < dim; k++)
      {
        CoordinateEvaluation f(k);
        std::vector<ctype> c(lfsf.size());
        lfsf.finiteElement().localInterpolation().interpolate(f, c);
        for (typename LFS::Traits::SizeType i = 0; i < lfsf.size(); i++)
          local_position[i][k] = c[i];
      }

      // determine local coordinates of vertices in father element
      std::vector<Dune::FieldVector<ctype, dim>> local_position_in_father(lfsf.size());
      for (typename LFS::Traits::SizeType i = 0; i < lfsf.size(); i++)
        local_position_in_father[i] = cell.geometryInFather().global(local_position[i]);

      // interpolation weights are values of coarse grid basis functions at fine grid points
      for (typename LFS::Traits::SizeType i = 0; i < lfsf.size(); i++)
      {
        typedef typename LFS::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType RangeType;
        std::vector<RangeType> phi(lfsc.size());
        lfsc.finiteElement().localBasis().evaluateFunction(local_position_in_father[i], phi);
        for (typename LFS::Traits::SizeType j = 0; j < lfsc.size(); j++)
        {
          if (phi[j] > 1E-6)
            graph[static_cast<size_type>(indexf[i])].insert(static_cast<size_type>(indexc[j]));
        }
      }
    }

    // now set up the sparse matrix pattern
    for (typename Graph::size_type i = 0; i < graph.size(); ++i)
      this->setrowsize(i, graph[i].size());
    this->endrowsizes();
    for (typename Graph::size_type i = 0; i < graph.size(); ++i)
    {
      for (typename IndexSet::iterator it = graph[i].begin(); it != graph[i].end(); ++it)
        this->addindex(i, *it);
    }
    this->endindices();

    // loop over grid for the second time and insert values
    for (const auto &cell : elements(gfsf.gridView()))
    {
      // bind local function spaces to element in fine and coarse grid
      lfsf.bind(cell);
      lfsf_cache.update();
      auto father = cell.father();
      lfsc.bind(father);
      lfsc_cache.update();

      // get global indices from the helper vector
      std::vector<E> indexf(lfsf.size());
      lviewf.bind(lfsf_cache);
      lviewf.read(indexf);
      lviewf.unbind();
      std::vector<E> indexc(lfsc.size());
      lviewc.bind(lfsc_cache);
      lviewc.read(indexc);
      lviewc.unbind();

      // determine local coordinates of vertices in fine element
      std::vector<Dune::FieldVector<ctype, dim>> local_position(lfsf.size());
      for (int k = 0; k < dim; k++)
      {
        CoordinateEvaluation f(k);
        std::vector<ctype> c(lfsf.size());
        lfsf.finiteElement().localInterpolation().interpolate(f, c);
        for (typename LFS::Traits::SizeType i = 0; i < lfsf.size(); i++)
          local_position[i][k] = c[i];
      }

      // determine local coordinates of vertices in father element
      std::vector<Dune::FieldVector<ctype, dim>> local_position_in_father(lfsf.size());
      for (typename LFS::Traits::SizeType i = 0; i < lfsf.size(); i++)
        local_position_in_father[i] = cell.geometryInFather().global(local_position[i]);

      // interpolation weights are values of coarse grid basis functions at fine grid points
      for (typename LFS::Traits::SizeType i = 0; i < lfsf.size(); i++)
      {
        typedef typename LFS::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType RangeType;
        std::vector<RangeType> phi(lfsc.size());
        lfsc.finiteElement().localBasis().evaluateFunction(local_position_in_father[i], phi);
        for (typename LFS::Traits::SizeType j = 0; j < lfsc.size(); j++)
          if (phi[j] > 1E-6)
            (*this)[static_cast<size_type>(indexf[i])][static_cast<size_type>(indexc[j])] = phi[j][0];
      }
    }
  }

  SpaceTimeProlongationOperator &operator=(const E &x)
  {
    BaseT::operator=(x);
    return *this;
  }

  // for debugging and AMG access
  BaseT &base()
  {
    return *this;
  }

  const BaseT &base() const
  {
    return *this;
  }

private:
  const GFSH &gfsh;
  int level;
};

/** Manage a hierarchy of prolongation operators for a given grid function space hierarchy
 */
template <typename GFSH>
class SpaceTimeProlongationOperatorHierarchy
{
public:
  // prolongation operator type
  typedef SpaceTimeProlongationOperator<GFSH> PO;

  SpaceTimeProlongationOperatorHierarchy(const GFSH &gfsh_)
      : gfsh(gfsh_), pmp(gfsh_.maxLevel() + 1)
  {
    for (int j = 1; j <= gfsh.maxLevel(); j++)
      pmp[j] = std::make_shared<PO>(gfsh, j);
  }

  // get a prolongation operator
  const PO &getPO(int level) const
  {
    if (level >= 1 && level < pmp.size())
      return *pmp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // get a prolongation operator
  const PO &operator[](int level) const
  {
    if (level >= 1 && level < pmp.size())
      return *pmp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // get a prolongation operator
  PO &getPO(int level)
  {
    if (level >= 1 && level < pmp.size())
      return *pmp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // get a prolongation operator
  PO &at(int time_i, int space_level, bool full)
  {
    if (full)
    {
      if (space_level >= 1 && space_level < pmp.size())
        return *pmp[space_level] ; //RLT;
      else
        DUNE_THROW(Dune::Exception, "level out of range");
    }
    else
    {
      //TODO(henrik): pt=0, so no change here
      //return time coarsening
      //Nt=1 => M_{tau_L}\tilde{M}^{1,2}_{tau_L}=[1]//because pt =0, so they are constant
      //=> return
      // I_N_L kromecker R^Lt=1
    }
  }

  int maxLevel() const
  {
    return gfsh.maxLevel();
  }

private:
  const GFSH &gfsh;
  std::vector<std::shared_ptr<PO>> pmp;
  // make copy constructor private to disallow copying
  SpaceTimeProlongationOperatorHierarchy(const SpaceTimeProlongationOperatorHierarchy &other) {}
};
/** Manage a hierarchy of stiffness matrices for a given grid function space hierarchy
 */
/** Manage a hierarchy of stiffness matrices for a given grid function space hierarchy
 */
template <typename GOSH>
class MatrixHierarchy
{
public:
  typedef typename GOSH::GO GO;
  typedef typename GOSH::MAT Matrix;

  MatrixHierarchy(const GOSH &gosh_)
      : gosh(gosh_), matrixp(gosh.maxLevel() + 1)
  {
    for (int l = 0; l <= gosh.maxLevel(); l++)
      matrixp[l] = std::make_shared<Matrix>(gosh.getGO(l));
  }

  // get a const matrix
  const Matrix &getMatrix(int level) const
  {
    if (level >= 0 && level < matrixp.size())
      return *matrixp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // get a matrix
  Matrix &getMatrix(int level)
  {
    if (level >= 0 && level < matrixp.size())
      return *matrixp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // get a const matrix
  const Matrix &at(int time_level, int space_level) const
  {
    if (space_level >= 0 && space_level < matrixp.size())
      return *matrixp[space_level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // get a matrix
  Matrix &operator[](int level)
  {
    if (level >= 0 && level < matrixp.size())
      return *matrixp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  int maxLevel() const
  {
    return gosh.maxLevel();
  }

  bool check() const
  {
    for (int l = 0; l < matrixp.size(); l++)
    {
      for (int i = 0; i < matrixp[l]->N(); i++)
        if ((*matrixp[l])[i][i] > 10.0)
        {
          std::cout << "found matrix element larger than 10, level=" << l << " index=" << i << " " << (*matrixp[l])[i][i] << std::endl;
          return true;
        }
    }
    return false;
  }

private:
  const GOSH &gosh;
  std::vector<std::shared_ptr<Matrix>> matrixp;

  // make copy constructor private to disallow copying
  MatrixHierarchy(const MatrixHierarchy &other) {}
};
/** Manage a hierarchy of degree of freedom vectors for a given grid function space hierarchy
 */
template <typename GFSH>
class VectorHierarchy
{
  // constants and types
  typedef typename GFSH::GFS GFS;

public:
  // constraints container type
  typedef typename GFSH::DOF Vector;

  VectorHierarchy(const GFSH &gfsh_) : gfsh(gfsh_), vectorp(gfsh_.maxLevel() + 1)
  {
    for (int j = 0; j <= gfsh.maxLevel(); j++)
      vectorp[j] = std::make_shared<Vector>(gfsh.getGFS(0, j));
  }

  // get a const vector
  const Vector &getVector(int level) const
  {
    if (level >= 0 && level < vectorp.size())
      return *vectorp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // get a vector
  Vector &getVector(int level)
  {
    if (level >= 0 && level < vectorp.size())
      return *vectorp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // get a const vector
  const Vector &operator[](int level) const
  {
    if (level >= 0 && level < vectorp.size())
      return *vectorp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  // get a vector
  Vector &operator[](int level)
  {
    if (level >= 0 && level < vectorp.size())
      return *vectorp[level];
    else
      DUNE_THROW(Dune::Exception, "level out of range");
  }

  int maxLevel() const
  {
    return gfsh.maxLevel();
  }

private:
  const GFSH &gfsh;
  std::vector<std::shared_ptr<Vector>> vectorp;
  // make copy constructor private to disallow copying
  VectorHierarchy(const VectorHierarchy &other) {}
};
#endif